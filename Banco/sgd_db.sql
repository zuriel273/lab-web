-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 11/08/2013 às 00h14min
-- Versão do Servidor: 5.5.32
-- Versão do PHP: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `sgd_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`id`, `nome`, `media`) VALUES
(1, 'CIÊNCIA DA COMPUTACÃO', 6.44444);

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

CREATE TABLE IF NOT EXISTS `disciplina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`id`, `nome`, `codigo`, `media`) VALUES
(1, 'COMPILADORES', 'MATA61', 6.66667),
(2, 'BANCO DE DADOS', 'MATA60', 3.33333),
(3, 'REDES DE COMPUTADORES I', 'MATA59', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `media_disciplina_professor`
--

CREATE TABLE IF NOT EXISTS `media_disciplina_professor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disciplina_id` int(11) NOT NULL,
  `pessoa_id` int(11) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `disciplina_id` (`disciplina_id`),
  KEY `pessoa_id` (`pessoa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `media_disciplina_professor`
--

INSERT INTO `media_disciplina_professor` (`id`, `disciplina_id`, `pessoa_id`, `media`) VALUES
(2, 2, 3, 3.33333),
(3, 3, 3, 6),
(4, 1, 3, 6.66667);

-- --------------------------------------------------------

--
-- Estrutura da tabela `media_semestre_curso`
--

CREATE TABLE IF NOT EXISTS `media_semestre_curso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curso_id` int(11) NOT NULL,
  `semestre` varchar(6) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `curso_id` (`curso_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `media_semestre_curso`
--

INSERT INTO `media_semestre_curso` (`id`, `curso_id`, `semestre`, `media`) VALUES
(1, 1, '2013.1', 6.44444);

-- --------------------------------------------------------

--
-- Estrutura da tabela `media_semestre_disciplina`
--

CREATE TABLE IF NOT EXISTS `media_semestre_disciplina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disciplina_id` int(11) NOT NULL,
  `semestre` varchar(6) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `disciplina_id` (`disciplina_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `media_semestre_disciplina`
--

INSERT INTO `media_semestre_disciplina` (`id`, `disciplina_id`, `semestre`, `media`) VALUES
(1, 3, '2013.1', 6),
(2, 1, '2013.1', 6.66667),
(3, 2, '2013.1', 3.33333);

-- --------------------------------------------------------

--
-- Estrutura da tabela `media_turma_aluno`
--

CREATE TABLE IF NOT EXISTS `media_turma_aluno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `turma_id` int(11) NOT NULL,
  `aluno_id` int(11) NOT NULL,
  `curso_id` int(11) NOT NULL,
  `semestre` varchar(6) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `turma_id` (`turma_id`),
  KEY `aluno_id` (`aluno_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `media_turma_aluno`
--

INSERT INTO `media_turma_aluno` (`id`, `turma_id`, `aluno_id`, `curso_id`, `semestre`, `media`) VALUES
(1, 1, 1, 1, '2013.1', 7),
(2, 2, 1, 1, '2013.1', 9),
(3, 3, 1, 1, '2013.1', 6),
(4, 1, 2, 1, '2013.1', 9),
(5, 2, 2, 1, '2013.1', 8),
(6, 3, 2, 1, '2013.1', 10),
(7, 1, 4, 1, '2013.1', 4),
(8, 2, 4, 1, '2013.1', 3),
(9, 3, 4, 1, '2013.1', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE IF NOT EXISTS `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL COMMENT '(0 - professor; 1 - aluno; 2 - administrador)',
  `curso_id` int(11) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `curso_id` (`curso_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`id`, `matricula`, `nome`, `senha`, `tipo`, `curso_id`, `media`) VALUES
(1, 11111, 'ALUNO 1', '11111', 1, 1, 7.33333),
(2, 22222, 'ALUNO 2', '22222', 1, 1, 9),
(3, 1, 'PROFESSOR 1', '1', 0, 0, 0),
(4, 33333, 'ALUNO 3', '33333', 1, 1, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `turma`
--

CREATE TABLE IF NOT EXISTS `turma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semestre` varchar(6) NOT NULL,
  `codigo` int(11) NOT NULL,
  `disciplina_id` int(11) NOT NULL,
  `pessoa_id` int(11) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `disciplina_id` (`disciplina_id`,`pessoa_id`),
  KEY `pessoa_id` (`pessoa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `turma`
--

INSERT INTO `turma` (`id`, `semestre`, `codigo`, `disciplina_id`, `pessoa_id`, `media`) VALUES
(1, '2013.1', 1, 1, 3, 6.66667),
(2, '2013.1', 1, 2, 3, 6.66667),
(3, '2013.1', 1, 3, 3, 6),
(4, '2013.1', 2, 2, 3, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `universidade`
--

CREATE TABLE IF NOT EXISTS `universidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `media` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `universidade`
--

INSERT INTO `universidade` (`id`, `nome`, `media`) VALUES
(1, 'UNIVERSIDADE FEDERAL DA BAHIA - UFBA', 6.44444);

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `media_semestre_curso`
--
ALTER TABLE `media_semestre_curso`
  ADD CONSTRAINT `media_semestre_curso_ibfk_1` FOREIGN KEY (`curso_id`) REFERENCES `curso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `media_semestre_disciplina`
--
ALTER TABLE `media_semestre_disciplina`
  ADD CONSTRAINT `media_semestre_disciplina_ibfk_1` FOREIGN KEY (`disciplina_id`) REFERENCES `turma` (`disciplina_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `media_turma_aluno`
--
ALTER TABLE `media_turma_aluno`
  ADD CONSTRAINT `media_turma_aluno_ibfk_1` FOREIGN KEY (`turma_id`) REFERENCES `turma` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `turma`
--
ALTER TABLE `turma`
  ADD CONSTRAINT `turma_ibfk_1` FOREIGN KEY (`disciplina_id`) REFERENCES `disciplina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `turma_ibfk_2` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
