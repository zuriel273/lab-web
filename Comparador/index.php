<!doctype html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Comparador de Faculdades - SGD</title>

	<link rel="stylesheet" type="text/css" href="stylesheets/styles.css">
	<script type="text/javascript" src="javascripts/bootstrap.js"></script>
</head>
<body>
	<div id="header" class="container">
			<h1>
				Comparador de Faculdades
			</h1>

	</div>

	<div id="selections" class="row-fluid">
		<div id="op1" class="span6 text-center">
			<select onChange="changeSection1(this)">
				<option>Escolha uma Faculdade</option>
				<option value="UFBA">UFBA - Federal da Bahia</option>
			</select>
		</div>
		<div id="op2" class="span6 text-center">
			<select onChange="changeSection2(this)">
				<option>Escolha uma Faculdade</option>
				<option value="UFBA">UFBA - Federal da Bahia</option>
			</select>
		</div>
	</div>

	<?php
		include 'functions.php';

		$faculdade1 = NULL;
		$faculdade2 = NULL;

		if(isset($_GET["f1"]) && isset($_GET["f2"])){
			$faculdade1 = loadSoap($_GET["f1"]);
			$faculdade2 = loadSoap($_GET["f2"]);
		}
	?>

	<?php if($faculdade1 && $faculdade2){?>
		<div id="main">
			<div id="uni1">
				<h2 class="f-name"><?= $faculdade2->nome ?> <span><?= $faculdade1->media ?></span></h2>

				<?php foreach($faculdade1->medias as $course){ ?>
					<div class="course"><?php echo $course->nome?></div>
					<div class="progress progress-striped">
					 	<div class="bar" style="width: <?= $course->media * 10 ?>%;"></div>
					</div>
					<div class="porcent"><?= $course->media?></div>
				<?php } ?>
			</div>

			 <div id="uni2">
				<h2 class="f-name"><span><?= $faculdade2->media ?></span> <?= $faculdade2->nome ?></h2>

				<?php foreach($faculdade2->medias as $course){ ?>
					<div class="course"><?php echo $course->nome?></div>
					<div class="progress progress-striped">
					 	<div class="bar" style="width: <?= $course->media * 10 ?>%;"></div>
					</div>
					<div class="porcent"><?= $course->media?></div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
	<script type="text/javascript">
		var op1 = 0
		var op2 = 0

		function changeSection1(obj){
			op1 = obj.value;

			if(op2 != 0){
				window.location.href = "<?php curPageURL() ?>" + "?f1=" + op1 + "&f2=" + op2;
			}
		}

		function changeSection2(obj){
			op2 = obj.value;

			if(op1 != 0){
				alert("<?= curPageURL() ?>" + "?f1=" + op1 + "&f2=" + op2);
				window.location.href = "<?php curPageURL() ?>" + "?f1=" + op1 + "&f2=" + op2;
			}
		}
	</script>
</body>
</html>