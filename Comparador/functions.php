<?php
	$lista = array(
		array("nome" => "UFBA", "url" => "http://localhost:8080/SGD/services/Media")
	);

	class Media {
        function Media($nome, $media){
            $this->nome = $nome;
            $this->media = $media;
        }
    }

    class Faculdade{
    	function Faculdade($nome, $media, $medias){
            $this->nome = $nome;
            $this->media = $media;
            $this->medias = $medias;
        }
    }

    function curPageURL() {
		$pageURL = 'http';

		// if ($_SERVER["HTTPS"] == "on"){
		// 	$pageURL .= "s";
		// }

		$pageURL .= "://";

		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"];
		}

		return $pageURL;
	}

	function loadSoap($name){
		global $lista;

		$url = "";

		foreach ($lista as $key){
			if($key["nome"] == $name){
				$url = $key["url"];
			}
		}

	    $client = new SoapClient('http://localhost:8080/SGD/services/Media?wsdl');

	    $function = 'getMedias';
	    $arguments= array();
	    $options = array('location' => 'http://localhost:8080/SGD/services/Media');

	    $result = $client->__soapCall($function, $arguments, $options);

	    $medias = array();
	    $count = count($result->getMediasReturn);
	    $i=2;

	    while($i < $count) {
	        $media = new Media($result->getMediasReturn[$i++], $result->getMediasReturn[$i++]);
	        array_push($medias, $media);
	    }

	    return new Faculdade($name, $result->getMediasReturn[1], $medias);
	}
?>
