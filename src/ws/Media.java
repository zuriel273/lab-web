package ws;

import java.util.ArrayList;
import model.Curso;
import dados.CursoDAO;
import model.Universidade;
import dados.UniversidadeDAO;

public class Media {
	public ArrayList<String> getMedias() {
		ArrayList<String> result = new ArrayList<String>();
		Universidade universidade = new UniversidadeDAO().busca(1);
		ArrayList<Curso> cursos = new CursoDAO().lista();
		
		result.add(universidade.getNome());
		result.add(String.format("%.2f", universidade.getMedia()));
		
		for(Curso curso : cursos) {
			result.add(curso.getNome());
			result.add(String.format("%.2f", curso.getMedia()));
		}
		return result;
	}
}