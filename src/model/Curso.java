package model;

public class Curso implements Cloneable {
    private int id;
    private String nome;
    private float media;
    private int rank;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getMedia() {
    	return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
    }

    public void setMedia(float media) {
        this.media = media;
    }
    
    public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	/**********************************************/

    @Override
    public String toString(){
        return this.nome;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == this) return true;
        if(obj == null) return false;
        if(!(obj instanceof Curso)) return false;
        if(((Curso) obj).getId() == this.id) return true;
        
        return false;
    }
    
    @Override
    public Curso clone() {
    	try {
    		return (Curso) super.clone();
    	}
    	catch(CloneNotSupportedException e) {
    		return null;
    	}
    }
}