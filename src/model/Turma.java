package model;

public class Turma {
    private int id, codigo;
    private String semestre;
    private Disciplina disciplina;
    private Pessoa professor;
    private float media;
    
    /*
     * Não podemos colocar uma array list de mediasAlunos aqui, senão o programa entra em loop
     */
    //private ArrayList<MediaAluno> mediasAlunos;
    
    public Turma() {
    	media = 0.0f;
    	//mediasAlunos = new ArrayList<MediaAluno>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public Disciplina getDisciplina() {
    	return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        System.out.println("Setando disciplina");
    	this.disciplina = disciplina;
    }

    public Pessoa getProfessor() {
        return professor;
    }

    public void setProfessor(Pessoa professor) {
        this.professor = professor;
    }

    public float getMedia() {
    	return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
    }

    public void setMedia(float media) {
        this.media = media;
    }
 /*   
    public void setMediasAlunos(ArrayList<MediaAluno> mediasAlunos) {
		this.mediasAlunos = mediasAlunos;
	}
    
    public ArrayList<MediaAluno> getMediasAlunos() {
		return mediasAlunos;
	}
*/    
    /**********************************************/

    @Override
    public String toString(){
        return this.codigo + ":" + this.semestre;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == this)
            return true;
        if(obj == null)
            return false;
        if(!(obj instanceof Turma))
            return false;
        if(((Turma) obj).getId() == this.id)
            return true;
        return false;
    }
}