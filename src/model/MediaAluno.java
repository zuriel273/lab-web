package model;

public class MediaAluno {
	private int id;
	private Aluno aluno; 
	private Turma turma;
	private float media;
	
	public MediaAluno() {
		aluno = new Aluno();
		turma = new Turma();
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public float getMedia() {
		return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
	}

	public void setMedia(float media) {
		this.media = media;
	}

	/**********************************************/

	@Override
    public String toString(){
        return this.turma + " - " + this.aluno;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == this)
            return true;
        if(obj == null)
            return false;
        if(!(obj instanceof MediaAluno))
            return false;
        if(((MediaAluno) obj).getId() == this.id)
            return true;
        return false;
    }
}
