package model;

public class Disciplina implements Cloneable {
    private int id;
    private String codigo, nome;
    private float media;
    private int rank;
	
    public Disciplina(){
    	media = 0f;
    }
    
    public int getId() {
		return id;
	}
	
    public void setId(int id) {
		this.id = id;
	}
	
    public String getCodigo() {
		return codigo;
	}
	
    public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
    public String getNome() {
		return nome;
	}
	
    public void setNome(String nome) {
		this.nome = nome;
	}
	
    public float getMedia() {
    	return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
	}
	
    public void setMedia(float media) {
		this.media = media;
	}
    
    
    public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	/**********************************************/

    @Override
    public String toString(){
    	return this.nome + " - " + this.codigo;
    }
    
    @Override
    public boolean equals(Object obj){
    	if(obj == this)
    		return true;
    	if(obj == null)
    		return false;
    	if(!(obj instanceof Disciplina))
    		return false;
    	if(((Disciplina) obj).getId() == this.id)
    		return true;
    	return false;
    }
    
    @Override
    public Disciplina clone() {
    	try {
    		return (Disciplina) super.clone();
    	}
    	catch(CloneNotSupportedException e) {
    		return null;
    	}
    }

}
