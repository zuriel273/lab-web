package model;

public class Pessoa {

    protected int id;
    protected String nome, senha, matricula;
    protected TipoPessoa tipo;
    protected float media;


	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public TipoPessoa getTipo() {
        return tipo;
    }

    public void setTipo(TipoPessoa tipo) {
        this.tipo = tipo;
    }

    public float getMedia() {
    	return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
	}

	public void setMedia(float media) {
		this.media = media;
	}

   /**********************************************/
    
    @Override
    public String toString(){
    	return this.nome;
    }
    
    @Override
    public boolean equals(Object obj){
    	if(obj == this)
    		return true;
    	if(obj == null)
    		return false;
    	if(!(obj instanceof Pessoa))
    		return false;
    	if(((Pessoa) obj).getId() == this.id)
    		return true;
    	return false;
    }
}