package model;

public class Universidade {
    private int id;
	private String nome;
    private float media;

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getMedia() {
        return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
    }

    public void setMedia(float media) {
        this.media = media;
    }
}