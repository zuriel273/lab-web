package model;

public enum TipoPessoa {
	/*
	 * OBSERVAÇÃO IMPORTANTE: NÃO ALTERE A ORDEM ABAIXO,
	 * SENÃO QUEBRA O BANCO DE DADOS
	 */
	PROFESSOR(0), ALUNO(1),	ADMINISTRADOR(2);
	
	private final int tipo;
	
	TipoPessoa(int tipo){
		this.tipo = tipo;
	}
	
	public int value(){
		return tipo;
	}
	
	static public TipoPessoa getTipoPessoa(int tipo){
		
		switch(tipo){
			case 0:
				return TipoPessoa.PROFESSOR;
			case 1:
				return TipoPessoa.ALUNO;
			case 2:
				return TipoPessoa.ADMINISTRADOR;
		}
		
		return null;
	}
}