package model;

public class MediaCurso {
	private int id;
	private Curso curso;
	private String semestre;
	private float media;
	
	public MediaCurso() {
		curso = new Curso();
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public float getMedia() {
		return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
	}

	public void setMedia(float media) {
		this.media = media;
	}
}
