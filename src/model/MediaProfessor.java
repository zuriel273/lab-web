package model;

public class MediaProfessor {
	private int id;
	private Pessoa professor;
	private Disciplina disciplina;
	private float media;
	
	public MediaProfessor() {
		professor = new Pessoa();
		disciplina = new Disciplina();
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getProfessor() {
		return professor;
	}

	public void setProfessor(Pessoa professor) {
		this.professor = professor;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public float getMedia() {
		return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
	}

	public void setMedia(float media) {
		this.media = media;
	}

	/**********************************************/

    @Override
    public String toString(){
        return this.professor.toString() + " - " + this.disciplina.toString();
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == this)
            return true;
        if(obj == null)
            return false;
        if(!(obj instanceof MediaProfessor))
            return false;
        if(((MediaProfessor) obj).getId() == this.id)
            return true;
        return false;
    }
}
