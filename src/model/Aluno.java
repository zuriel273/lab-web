package model;

public class Aluno extends Pessoa{
    private Curso curso;

    public Aluno(){
        this.tipo = TipoPessoa.ALUNO;
    }

    public Curso getCurso(){
        return curso;
    }
    
    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    /**********************************************/

    @Override
    public String toString(){
        return this.nome;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == this)
            return true;
        if(obj == null)
            return false;
        if(!(obj instanceof Aluno))
            return false;
        if(((Aluno) obj).getId() == this.id)
            return true;
        return false;
    }
}