package model;

public class MediaDisciplina {
	private int id;
	private Disciplina disciplina;
	private String semestre;
	private float media;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public String getSemestre() {
		return semestre;
	}
	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	public float getMedia() {
		return Float.parseFloat(String.format("%.2f", media).replace(",", "."));
	}
	public void setMedia(float media) {
		this.media = media;
	}
}
