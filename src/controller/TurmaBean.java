package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import dados.DisciplinaDAO;
import dados.PessoaDAO;
import dados.TurmaDAO;
import model.Aluno;
import model.Turma;

@SessionScoped
@ManagedBean
public class TurmaBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Turma turma;
	private Aluno aluno;	
	private ArrayList<Turma> lista;
	private List<Turma> filteredTurmas;
	private ArrayList<Aluno> listaAlunos;
	private int matricula;
	
	
	public TurmaBean(){
		this.turma = new Turma();
		this.turma.setCodigo(0001);
		this.aluno = new Aluno();
		listarAlunos();
		listarTurmas();
	}
	
	private void atualizarLista() {
		TurmaDAO turmaDAO = new TurmaDAO();
		lista = turmaDAO.lista();
	}
	
    public List<Turma> getFilteredTurmas() {  
        return filteredTurmas;  
    }  
  
    public void setFilteredTurmas(List<Turma> filteredTurmas) {  
        this.filteredTurmas = filteredTurmas;
    } 
	
	public String adicionaTurma(){
		TurmaDAO turmadao = new TurmaDAO();
		
		if(turmadao.adiciona(turma)){
			atualizarLista();
			turma = new Turma();
			FacesContext.getCurrentInstance(). addMessage("confirm:message", new FacesMessage("Turma cadastrado com sucesso!"));
		}else{
			FacesContext.getCurrentInstance(). addMessage("", new FacesMessage("Erro ao cadastra da turma."));
		}
		return "/Cadastro/turma.jsf";
	}
	
	public String carregar(){
		
		System.out.println("Carregando turma");		
		return "../Editar/turma.xhtml";		
	}
	
	public String carregarAlunos(){
		listarAlunos();
		System.out.println("Carregando Alunos");
		
		return "../Cadastro/alunoTurma.jsf?faces-redirect=true";		
	}
	
	public String adicionaAluno(){
		TurmaDAO turmadao = new TurmaDAO();
		PessoaDAO a = new PessoaDAO();
		aluno =(Aluno) a.buscaPorMatricula(matricula);
		if (aluno != null){
			if(turmadao.adicionaAlunoTurma(turma.getId(), aluno.getId(), aluno.getCurso().getId(), turma.getSemestre(), 0)){
				//salvo com sucesso	
			}else{
				//matricula jah cadastrada
			}
			
		}else{
			//matricula inexistente
		}
		listarAlunos();
		return "../Cadastro/alunoTurma.xhtml";
	}
	
	public String excluiTurmaAluno(){
		
		TurmaDAO turmadao = new TurmaDAO();
		
		System.out.println(aluno.getId()+ aluno.getMatricula());
		System.out.println("Carregando Alunos Ex");
		turmadao.excluiAlunoTurma(turma.getId(), aluno.getId());
		
		return listarAlunos();
	}
	
	public void atualizarListaAluno() {
		TurmaDAO turmaDAO = new TurmaDAO();
		listaAlunos = turmaDAO.listaAlunos(turma.getId());
//		listaAlunos = turmaDAO.listaAlunos(1);
	}
	
	public String listarAlunos() {
		atualizarListaAluno();
		return "../Cadastro/alunoTurma.xhtml";
	}
	
	public String alterar(){
		System.out.println("Alterando turma");
		TurmaDAO turmaDAO = new TurmaDAO();
		turmaDAO.altera(turma);
		return listarTurmas();		
	}
	
	public String listarTurmas() {
		atualizarLista();
		return "../Listagem/turma.xhtml";
	}

	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public void setTurma(Turma turma){this.turma = turma; };
	public Turma getTurma(){return turma; };
	
	public void setAluno(Aluno aluno){this.aluno = aluno; };
	public Aluno getAluno(){return aluno; };
	
	public void setMatricula(int matricula){this.matricula = matricula; };
	public int getMatricula(){return matricula; };
	
	public ArrayList<Turma> getLista(){return lista; };	
	public ArrayList<Aluno> getListaAlunos(){return listaAlunos; };	
}
