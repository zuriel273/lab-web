package controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Universidade;
import dados.UniversidadeDAO;

@ManagedBean
@SessionScoped
public class UniversidadeBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Universidade universidade;
	
	public UniversidadeBean(){
		atualizar();
	}
	
	private void atualizar() {
		universidade = new UniversidadeDAO().busca(1);
	}
	
		
	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public Universidade getUniversidade() {
		atualizar();
		return universidade;
	}

	public void setUniversidade(Universidade universidade) {
		this.universidade = universidade;
	}
}
