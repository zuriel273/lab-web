package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import dados.Calculo;
import dados.MediaAlunoDAO;
import dados.MediaProfessorDAO;
import dados.PessoaDAO;
import dados.TurmaDAO;
import dados.UniversidadeDAO;
import model.Curso;
import model.MediaAluno;
import model.MediaProfessor;
import model.Pessoa;
import model.TipoPessoa;
import model.Turma;
import model.Universidade;

@ManagedBean
@SessionScoped
public class ProfessorBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{sessionBean.autorizado}")
	private Pessoa loged;
	
	private Pessoa professor;
	private Turma selectedTurma;
	private MediaAluno selectedNota;
	
	private ArrayList<Pessoa> lista;
	private ArrayList<Turma> turmas;
	private List<Turma> filteredTurmas;
	private List<Pessoa> filteredProfessores;
	private ArrayList<MediaAluno> notas;
	private ArrayList<MediaProfessor> mediasProfessor;
	
	public ProfessorBean() {
		professor = new Pessoa();
		atualizarLista();
	}
	
	public String alterar(){
		System.out.println("Alterando disciplina:" + professor.getNome());
		
		PessoaDAO professorDAO = new PessoaDAO();
		professorDAO.altera(professor);
		
		return buscarProfessores();		
	}

	public String buscarProfessores() {
		atualizarLista();				
		return "../Listagem/professor.xhtml";
	}	
	
	public String mostrarGrafico() {
		mediasProfessor = new MediaProfessorDAO().lista(loged);
		return "/Grafico/professor.jsf";
	}
	
	private void atualizarLista() {
		PessoaDAO pessoadao = new PessoaDAO();
		this.lista = pessoadao.lista(TipoPessoa.PROFESSOR);
	}
	
	public String salvar(){
		
		professor.setTipo(TipoPessoa.PROFESSOR);
		
		System.out.println(professor.getNome());
		
		PessoaDAO pessoaDAO = new PessoaDAO();
		
		if(pessoaDAO.adiciona(professor)){
			professor = new Pessoa();
			atualizarLista();
			FacesContext.getCurrentInstance(). addMessage("confirm:message", new FacesMessage("Professor cadastrado com sucesso!"));
		}else{
			FacesContext.getCurrentInstance(). addMessage("", new FacesMessage("Erro ao cadastra do professor."));	
		}
		
		return "default.xhtml";		
	}
	
	public Pessoa buscaId(int id){
		Pessoa pessoa;		
		PessoaDAO pessoaDAO = new PessoaDAO();
		pessoa = pessoaDAO.busca(id);
		
		return pessoa;		
	}
	
	public String selecionar() {
		//this.professor = new SessionBean().getAutorizado();
		System.out.println(loged.getNome());
		TurmaDAO t = new TurmaDAO();
		turmas = t.lista(loged);
		return "/Listagem/turmasProfessor.xhtml";
	}
	
	public String carregar() {
		//this.professor = new SessionBean().getAutorizado();
		System.out.println("Carregando Professor: " + professor.getMatricula() +" Nome: "+ professor.getNome()); 
		return "../Editar/professor.xhtml";
	}
	
	public String listarNotas() {		
		MediaAlunoDAO md = new MediaAlunoDAO();
		System.out.print(selectedTurma);
		notas = md.buscaAlunos(selectedTurma.getId());
		return "../Editar/nota.xhtml";
	}
	
	public String salvarNotas() {
		
		//Atualiza notas informadas pelo professor
		MediaAlunoDAO mad = new MediaAlunoDAO();
		for(MediaAluno nota : notas)
			mad.altera(nota);
		
		Calculo calculo = new Calculo();
		
		//Atualiza média da turma
		calculo.calcularMedia(selectedTurma);
		
		//Atualiza média dos alunos
		for(int i=0; i<notas.size();i++)
			calculo.calcularMedia(notas.get(i).getAluno());
		
		//Atualiza média do professor
		calculo.calcularMedia(loged, selectedTurma.getDisciplina());
		
		//Atualiza média da disciplina
		calculo.calcularMedia(selectedTurma.getDisciplina());
		
		//Atualiza média da disciplina no semestre
		calculo.calcularMedia(selectedTurma.getDisciplina(), selectedTurma.getSemestre());

		//Atualiza média dos cursos relacionados
		ArrayList<Curso> cursos = new ArrayList<Curso>();
		for(int i=0; i<notas.size();i++) {
			Curso curso = notas.get(i).getAluno().getCurso();
			if(!cursos.contains(curso))
				cursos.add(curso);
		}
		for(Curso curso : cursos)
			calculo.calcularMedia(curso);
		
		//Atualiza média dos cursos relacionados no semestre
		for(Curso curso : cursos)
			calculo.calcularMedia(curso, selectedTurma.getSemestre());
		
		//Atualiza média da universidade
		Universidade u = new UniversidadeDAO().busca(1);
		calculo.calcularMedia(u);
		
		return listarNotas();
	}
	
	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public void setProfessor(Pessoa professor){this.professor = professor; };
	public Pessoa getProfessor(){return professor; };
	
	public Pessoa getLoged() {return loged;}
	public void setLoged(Pessoa loged) {this.loged = loged;}
	
	public ArrayList<Pessoa> getLista(){return lista; }
	
	public ArrayList<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(ArrayList<Turma> turmas) {
		this.turmas = turmas;
	}
	
	public Turma getSelectedTurma() {
		return selectedTurma;
	}

	public void setSelectedTurma(Turma selectedTurma) {
		this.selectedTurma = selectedTurma;
	}
	
	public List<Pessoa> getFilteredProfessores() {  
        return filteredProfessores;  
    }  
  
    public void setFilteredProfessores(List<Pessoa> filteredProfessores) {  
        this.filteredProfessores = filteredProfessores;
    }
	
    public List<Turma> getFilteredTurmas() {  
        return filteredTurmas;  
    }  
  
    public void setFilteredTurmas(List<Turma> filteredTurmas) {  
        this.filteredTurmas = filteredTurmas;
    }
    
	public ArrayList<MediaAluno> getNotas() {
		return notas;
	}

	public void setNotas(ArrayList<MediaAluno> notas) {
		this.notas = notas;
	}
	
	public MediaAluno getSelectedNota() {
		return selectedNota;
	}

	public void setSelectedNota(MediaAluno selectedNota) {
		this.selectedNota = selectedNota;
	}
	
	public ArrayList<MediaProfessor> getMediasProfessor() {
		return mediasProfessor;
	}
}
