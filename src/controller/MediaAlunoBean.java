package controller;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import dados.MediaAlunoDAO;
import dados.PessoaDAO;
import dados.TurmaDAO;
import model.Aluno;
import model.Disciplina;
import model.MediaAluno;
import model.Turma;

@ManagedBean
@SessionScoped
public class MediaAlunoBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Turma turma;
	private ArrayList<MediaAluno> lista;
	private int id_aluno;
	private int id_turma;
	private int id_mediaAluno;
	private float media;
		
	/****************************************/
	
	public String salvar(){
		Aluno aluno = (Aluno)new PessoaDAO().busca(id_aluno);
		System.out.print("AQUI");
		Turma turma = new TurmaDAO().busca(id_turma);
		System.out.println("Salvando media: Aluno = " + aluno.getNome()+" Turma = "+turma.getSemestre()+"-"+"Média = "+media);
		MediaAluno mediaAluno = new MediaAluno();
		mediaAluno.setAluno(aluno);
		mediaAluno.setTurma(turma);
		mediaAluno.setMedia(media);
		new MediaAlunoDAO().adiciona(mediaAluno);
		return "index_teste_mediaAluno.xhtml";
	}
	
	public String alterar(){
		MediaAlunoDAO md = new MediaAlunoDAO();
		MediaAluno m = new MediaAluno();
		m.setId(id_mediaAluno);
		m.setMedia(media);
		md.altera(m);
		return "index_teste_mediaAluno.xhtml";
	}
	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public int getId_aluno() {
		return id_aluno;
	}

	public void setId_aluno(int id_aluno) {
		this.id_aluno = id_aluno;
	}

	public int getId_turma() {
		return id_turma;
	}

	public void setId_turma(int id_turma) {
		this.id_turma = id_turma;
	}

	public float getMedia() {
		return media;
	}

	public void setMedia(float media) {
		this.media = media;
	}

	public int getId_mediaAluno() {
		return id_mediaAluno;
	}

	public void setId_mediaAluno(int id_mediaAluno) {
		this.id_mediaAluno = id_mediaAluno;
	}	
}