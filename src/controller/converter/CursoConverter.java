package controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import controller.CursosBean;
import model.Curso;

@FacesConverter("cursoConverter")  
public class CursoConverter implements Converter {  
      
    @Override  
    public Object getAsObject(FacesContext context, UIComponent component, String value) {  
        if(value != ""){
            CursosBean cursoBean = new CursosBean();
            return cursoBean.buscaId(Integer.parseInt(value));  
        }
        
        return null;
    }  
  
    @Override  
    public String getAsString(FacesContext context, UIComponent component, Object object) {  
        if(object != null && object instanceof Curso) {      
            return ""+((Curso)object).getId();     
        }     
        
        return "";       
    } 
}
