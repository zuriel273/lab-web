package controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import controller.DisciplinaBean;
import model.Disciplina;

@FacesConverter("disciplinaConverter")   
public class DisciplinaConverter implements Converter {  
      
    @Override  
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println(component.getAttributes());

    	if(value != ""){
    		DisciplinaBean disciplinaBean = new DisciplinaBean(); 
        	return disciplinaBean.buscaId(Integer.parseInt(value));
    	}
    	
    	return null;
    }  
  
    @Override  
    public String getAsString(FacesContext context, UIComponent component, Object object) {     	
        if(object != null && object instanceof Disciplina) {      
        	return ""+((Disciplina)object).getId();     
        }     
        
        return "";       
    }   
}  
