package controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import controller.AlunoBean;
import model.Aluno;

@FacesConverter("alunoConverter")  
public class AlunoConverter implements Converter {  
      
    @Override  
    public Object getAsObject(FacesContext context, UIComponent component, String value) {  
        /*if(value != ""){
            AlunoBean alunoBean = new AlunoBean();
            return alunoBean.buscaId(Integer.parseInt(value));  
        }*/
        
        return null;
    }  
  
    @Override  
    public String getAsString(FacesContext context, UIComponent component, Object object) {  
        if(object != null && object instanceof Aluno) {      
            return ""+((Aluno) object).getId();     
        }     
        
        return "";       
    } 
}  
