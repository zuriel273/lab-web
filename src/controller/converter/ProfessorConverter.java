package controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import controller.ProfessorBean;
import model.Pessoa;

@FacesConverter("professorConverter")   
public class ProfessorConverter implements Converter {  
	
    @Override  
    public Object getAsObject(FacesContext context, UIComponent component, String value) {    	
    	if(value != ""){
	    	ProfessorBean professorBean = new ProfessorBean();
	        return professorBean.buscaId(Integer.parseInt(value));  
    	}
    	
    	return null;
    }  
  
    @Override  
    public String getAsString(FacesContext context, UIComponent component, Object object) {  
        if(object != null && object instanceof Pessoa) {      
            return ""+((Pessoa)object).getId();     
        }      
        return "1"; 
    }    
}  
