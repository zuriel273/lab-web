package controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import controller.MediaAlunoBean;
import model.MediaAluno;

@FacesConverter("mediaAlunoConverter")  
public class MediaAlunoConverter implements Converter {  
      
    @Override  
    public Object getAsObject(FacesContext context, UIComponent component, String value) {  
        /*if(value != ""){
            MediaAlunoBean mediaAlunoBean = new MediaAlunoBean();
            return mediaAlunoBean.buscaId(Integer.parseInt(value));  
        }
        */
        return null;
    }  
  
    @Override  
    public String getAsString(FacesContext context, UIComponent component, Object object) {  
        if(object != null && object instanceof MediaAluno) {      
            return ""+((MediaAluno) object).getId();     
        }     
        
        return "";       
    } 
}  
