package controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import controller.TurmaBean;
import model.Turma;

@FacesConverter("turmaConverter")  
public class TurmaConverter implements Converter {  
      
    @Override  
    public Object getAsObject(FacesContext context, UIComponent component, String value) {  
    	/*if(value != ""){
            TurmaBean turmaBean = new TurmaBean();
            return turmaBean.buscaId(Integer.parseInt(value));  
        }
        */
        return null;
    }  
  
    @Override  
    public String getAsString(FacesContext context, UIComponent component, Object object) {  
        if(object != null && object instanceof Turma) {      
            return ""+((Turma)object).getId();     
        }     
        
        return "";       
    } 
}  
