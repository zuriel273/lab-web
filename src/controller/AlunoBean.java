package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.swing.JOptionPane;

import com.sun.org.apache.bcel.internal.generic.ARRAYLENGTH;

import dados.CursoDAO;
import dados.MediaAlunoDAO;
import dados.PessoaDAO;
import model.Aluno;
import model.Curso;
import model.MediaAluno;
import model.MediaCurso;
import model.Pessoa;
import model.TipoPessoa;

@ManagedBean
@SessionScoped
public class AlunoBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{sessionBean.autorizado}")
	private Pessoa loged;
	private ArrayList<Pessoa> lista;
	private List<Pessoa> filteredAlunos;
	private Aluno aluno;
	private ArrayList<MediaAluno> medias;
	private String semestre;
	private ArrayList<String> semestres;
	
	/****************************************/
	
	public AlunoBean() {
		aluno = new Aluno();
		semestre = "GERAL";
		setSemestres();
		atualizarLista();
	}
	
	private void atualizarLista() {
		PessoaDAO pessoadao = new PessoaDAO();
		System.out.println("Alterando teste:" );
		this.lista = pessoadao.lista(TipoPessoa.ALUNO);
	}
	
	public String alterar(){
		System.out.println("Alterando disciplina:" + aluno.getNome());
		
		PessoaDAO alunoDAO = new PessoaDAO();
		alunoDAO.altera(aluno);
		
		return buscarAlunos();		
	}

	public String buscarAlunos() {
		atualizarLista();				
		return "../Listagem/aluno.xhtml";
	}	
	
	public String carregar() {
		//this.professor = new SessionBean().getAutorizado();
		System.out.println("Carregando Professor: " + aluno.getMatricula() +" Nome: "+ aluno.getNome()); 
		return "../Editar/aluno.xhtml";
	}
	
	private void setSemestres() {
		medias = new MediaAlunoDAO().lista();
		semestres = new ArrayList<String>();
		for(MediaAluno m : medias) {
			int i;
			for(i=semestres.size()-1; i>=0 && semestres.get(i).compareTo(m.getTurma().getSemestre()) < 0; i--);
			if(i < 0 || semestres.get(i).compareTo(m.getTurma().getSemestre()) != 0)
				semestres.add(i+1, m.getTurma().getSemestre());
		}
		semestres.add(0,"GERAL");
	}
	
	public String mostrarGrafico() {
		medias = new MediaAlunoDAO().lista((Aluno)loged);
		return "/Grafico/aluno.jsf?faces-redirect=true";
	}
	
	public String listarPorSemestre(ValueChangeEvent event) {
		if(event != null)
			semestre = (String) event.getNewValue();
		if(semestre.equals("GERAL"))
			this.medias = new MediaAlunoDAO().lista((Aluno) loged);
		else
			this.medias = new MediaAlunoDAO().lista((Aluno) loged, semestre);
		return "/Grafico/aluno.jsf?faces-redirect=true";
	}

	
	public String salvar(){
		System.out.println("Salvando aluno:" + aluno.getNome());
		PessoaDAO pessoaDAO = new PessoaDAO();
		if(pessoaDAO.adiciona(aluno)){
			this.aluno = new Aluno();
			atualizarLista();
			FacesContext.getCurrentInstance(). addMessage("confirm:message", new FacesMessage("Aluno cadastrado com sucesso!"));
		}else{
			FacesContext.getCurrentInstance(). addMessage("", new FacesMessage("Erro ao cadastra o aluno."));	
		}
		
		return "/Cadastro/aluno.xhtml";	
	}
	
	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public void setAluno(Aluno aluno){this.aluno = aluno; };
	public Aluno getAluno(){return aluno; };
	
	public Pessoa getLoged() {return loged;}
	public void setLoged(Pessoa loged) {this.loged = loged;}

	public ArrayList<MediaAluno> getMedias() {
		return medias;
	}
	
	public List<Pessoa> getFilteredAlunos() {  
        return filteredAlunos;  
    }  
  
    public void setFilteredAlunos(List<Pessoa> filteredAlunos) {  
        this.filteredAlunos = filteredAlunos;
    }
	
	
	public ArrayList<Pessoa> getLista(){return lista; }
	
	public void setMedias(ArrayList<MediaAluno> medias) {
		this.medias = medias;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public ArrayList<String> getSemestres() {
		return semestres;
	}

	public void setSemestres(ArrayList<String> semestres) {
		this.semestres = semestres;
	}
}
