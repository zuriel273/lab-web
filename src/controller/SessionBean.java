package controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

import dados.PessoaDAO;
import model.Aluno;
import model.Pessoa;

@ManagedBean(name="sessionBean")
@SessionScoped
public class SessionBean {
	private String login;
	private String senha;
	private Pessoa autorizado = null;
	
	public String logar() {
		String target = "Index";
		
		PessoaDAO pdao = new PessoaDAO();
		Pessoa test = pdao.autorizar(login, senha);
		
		if (test != null) {
			autorizado = test;
			target = "loged?faces-redirect=true";
			
		} else {
			autorizado = null;
			target = "login";
			FacesContext.getCurrentInstance(). addMessage("login:btnLogin", new FacesMessage("login/senha incorreto"));
		}

		return target;
	}
	
	public String logout() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) 
		fc.getExternalContext().getSession(false);
		session.invalidate();
		
		autorizado = null;
		return "/default.xhtml?faces-redirect=true";
	}
		
	/****************************************
	 * EVENTOS
	 ****************************************/
	public void verificarAutorizacao(ComponentSystemEvent e){
		System.out.println("N�o atorizado");
		
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("/SGD/access_denied.xhtml");
		} catch (IOException ex) {
			Logger.getLogger(Pessoa.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/***
	 * Somente para Administrador
	 */
	public void nivel1(ComponentSystemEvent e){
		System.out.println("N�o atorizado: Pagina Nivel 1");
		if (autorizado == null || autorizado.getTipo().value() != 2) {
			verificarAutorizacao(e);
		}
	}
	
	/***
	 * Somente para Professor
	 */
	public void nivel2(ComponentSystemEvent e){
		if (autorizado == null || autorizado.getTipo().value() != 0) {
			verificarAutorizacao(e);
		}
	}
	
	/***
	 * Somente para Aluno
	 */
	public void nivel3(ComponentSystemEvent e){
		if (autorizado == null || autorizado.getTipo().value() != 1) {
			verificarAutorizacao(e);
		}
	}
	
	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public String getLogin(){return login;}
	public void setLogin(String login){this.login = login;}
	
	public String getSenha(){return senha;}
	public void setSenha(String senha){this.senha = senha;}
	
	public Pessoa getAutorizado(){return autorizado;}
	public Aluno getAluno(){
		return (Aluno) autorizado;
	}
}
