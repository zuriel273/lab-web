package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import dados.CursoDAO;
import dados.HibernateUtil;
import dados.MediaCursoDAO;
import model.Aluno;
import model.Curso;
import model.MediaCurso;


@ManagedBean
@SessionScoped
public class CursosBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Curso curso;
	private ArrayList<Curso> lista;
	private List<Curso> filteredCursos;
	private String semestre;
	private String ordem;
	private ArrayList<String> semestres;
	private final ArrayList<String> ordens;
	ArrayList<MediaCurso> mc;
	
	public CursosBean(){
		//N�o alterar ordens, sen�o quebra o c�digo
		ordens = new ArrayList<String>();
		ordens.add("ALFABETICA");
		ordens.add("RANK");
		ordem = "RANK";
		semestre = "GERAL";
		this.curso = new Curso();
		setSemestres();
		listarPorSemestre(null);
	}
	
	/****************************************/
	
	private void setSemestres() {
		mc = new MediaCursoDAO().lista();
		semestres = new ArrayList<String>();
		for(MediaCurso m : mc) {
			int i;
			for(i=semestres.size()-1; i>=0 && semestres.get(i).compareTo(m.getSemestre()) < 0; i--);
			if(i < 0 || semestres.get(i).compareTo(m.getSemestre()) != 0) semestres.add(i+1, m.getSemestre());
		}
		semestres.add(0,"GERAL");
	}
	
	
	public String listarPorSemestre(ValueChangeEvent event) {
		if(event != null)
			semestre = (String) event.getNewValue();
		if(semestre.equals("GERAL"))
			atualizarLista();
		else {
			this.lista = new ArrayList<Curso>();
			mc = new MediaCursoDAO().lista(semestre);
			int i = 1;
			for(MediaCurso m : mc) {
				m.getCurso().setMedia(m.getMedia());
				m.getCurso().setRank(i++);
				this.lista.add(m.getCurso());
			}
		}
		return ordenar(null);
	}
	
	public String ordenar(ValueChangeEvent event) {
		if(event != null) {
			ordem = (String) event.getNewValue();
		}
		if(ordem.equals(ordens.get(0)))
			for(int i=1, j; i<lista.size(); i++) {
				Curso c = lista.get(i).clone();
				for(j=i-1; j>=0 && c.getNome().compareTo(lista.get(j).getNome()) < 0; j--) {
					lista.set(j+1, lista.get(j));
				}
				lista.set(j+1, c);
			}
		else {
			for(int i=1, j; i<lista.size(); i++) {
				Curso c = lista.get(i).clone();
				for(j=i-1; j>=0 && c.getRank() < lista.get(j).getRank(); j--) {
					lista.set(j+1, lista.get(j));
				}
				lista.set(j+1, c);
			}
			
		}
		return "/SGD/default.jsf";
	}
	
	private void atualizarLista() {
		Transaction trns = null;  
		Session session = null;
		session = HibernateUtil.getSession().openSession();		
		trns = session.beginTransaction();
		this.lista = (ArrayList<Curso>) session.createQuery("from Curso as u order by u.media desc").list();
		int i = 1;
		for(Curso curso : lista) curso.setRank(i++);
	}
	
	public String salvar(){
		System.out.println("Salvando curso:" + curso.getNome());
		
		Session session = null;
		Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
		curso.setMedia(0);
		
		try{  
			trns = session.beginTransaction();       
			session.save(curso);       
			session.getTransaction().commit(); 
		}catch(RuntimeException e){	
	    	if(trns != null){
	    		trns.rollback();
	    	}  
	    	e.printStackTrace();
	    }finally{
			session.flush();
			session.close();   
		}
		
		curso = new Curso();
		atualizarLista();
		
		FacesContext.getCurrentInstance(). addMessage("confirm:message", new FacesMessage("Curso cadastrado com sucesso!"));
	
		return "/Cadastro/curso.jsf";		
	}
	
	public String carregar(){
		System.out.println("Carregando Curso:" + curso.getNome());		
		return "../Editar/curso.xhtml";		
	}

	public String alterar(){
		System.out.println("Alterando disciplina:" + curso.getNome());
		
		System.out.println("Alterando disciplina:" + curso.getId());
		
		Transaction trns = null;  
		Session session = HibernateUtil.getSession().openSession();
								
		try {
			trns = session.beginTransaction();   
		    String hqlUpdate = "update Curso u set u.nome = :newName where u.id = :oldId";
		    int updatedEntities = session.createQuery( hqlUpdate )   
		    		 .setString( "newName", curso.getNome() )  
		    		 .setInteger( "oldId", curso.getId() )   
		    		 .executeUpdate();    
		     trns.commit();  
		     
		     
		} catch (RuntimeException e){
			
			if(trns != null){    trns.rollback();   }  
		     e.printStackTrace();  } 
		    finally{   session.flush();   session.close();  
		    
		    } 		
		
		
		return buscarCursos();		
	}

	public String buscarCursos() {
		atualizarLista();				
		return "../Listagem/curso.xhtml";
	}	
	
	public Curso buscaId(int id){
		
		Session session = null;
		Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
		
		trns = session.beginTransaction();
		Curso curso;		
		curso = (Curso)session.createQuery("from Curso as u where u.id = :oldId").setInteger("oldId", id).uniqueResult();
		
		return curso;		
	}
	
	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public void setCurso(Curso curso){this.curso = curso; };
	public Curso getCurso(){return this.curso; };
	
	public ArrayList<Curso> getLista() {return this.lista; }
	
	public List<Curso> getFilteredCursos() {
		return filteredCursos;
	}

	public void setFilteredCursos(List<Curso> filteredCursos) {
		this.filteredCursos = filteredCursos;
	}
	
	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public ArrayList<String> getSemestres() {
		return semestres;
	}

	public void setSemestres(ArrayList<String> semestres) {
		this.semestres = semestres;
	}

	public String getOrdem() {
		return ordem;
	}

	public void setOrdem(String ordem) {
		this.ordem = ordem;
	}

	public ArrayList<String> getOrdens() {
		return ordens;
	}
}