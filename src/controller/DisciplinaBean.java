package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.swing.JOptionPane;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import dados.DisciplinaDAO;
import dados.HibernateUtil;
import dados.MediaCursoDAO;
import dados.MediaDisciplinaDAO;
import model.Curso;
import model.Disciplina;
import model.MediaCurso;
import model.MediaDisciplina;

@ManagedBean
@SessionScoped
public class DisciplinaBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Disciplina disciplina;
	private ArrayList<Disciplina> lista;
	private List<Disciplina> filteredDisciplinas;
	private ArrayList<String> semestres;
	private final ArrayList<String> ordens;
	private String semestre;
	private String ordem;
	private ArrayList<MediaDisciplina> md;
	
	public DisciplinaBean(){
		ordens = new ArrayList<String>();
		ordens.add("ALFABETICA");
		ordens.add("RANK");
		ordem = "RANK";
		semestre = "GERAL";
		this.disciplina = new Disciplina();
		setSemestres();
		listarPorSemestre(null);
		System.out.println(lista.size());
	}
	
	/****************************************/
	
	private void setSemestres() {
		md = new MediaDisciplinaDAO().lista();
		semestres = new ArrayList<String>();
		for(MediaDisciplina m : md) {
			int i;
			for(i=semestres.size()-1; i>=0 && semestres.get(i).compareTo(m.getSemestre()) < 0; i--);
			if(i < 0 || semestres.get(i).compareTo(m.getSemestre()) != 0) semestres.add(i+1, m.getSemestre());
		}
		semestres.add(0,"GERAL");
	}
	
	
	public String listarPorSemestre(ValueChangeEvent event) {
		if(event != null)
			semestre = (String) event.getNewValue();
		if(semestre.equals("GERAL"))
			atualizarLista();
		else {
			this.lista = new ArrayList<Disciplina>();
			md = new MediaDisciplinaDAO().lista(semestre);
			int i = 1;
			for(MediaDisciplina m : md) {
				m.getDisciplina().setMedia(m.getMedia());
				m.getDisciplina().setRank(i++);
				this.lista.add(m.getDisciplina());
			}
		}
		return ordenar(null);
	}
	
	public String ordenar(ValueChangeEvent event) {
		if(event != null) {
			ordem = (String) event.getNewValue();
		}
		if(ordem.equals(ordens.get(0)))
			for(int i=1, j; i<lista.size(); i++) {
				Disciplina d = lista.get(i).clone();
				for(j=i-1; j>=0 && d.getNome().compareTo(lista.get(j).getNome()) < 0; j--) {
					lista.set(j+1, lista.get(j));
				}
				lista.set(j+1, d);
			}
		else {
			for(int i=1, j; i<lista.size(); i++) {
				Disciplina d = lista.get(i).clone();
				for(j=i-1; j>=0 && d.getRank() < lista.get(j).getRank(); j--) {
					lista.set(j+1, lista.get(j));
				}
				lista.set(j+1, d);
			}
			
		}
		return "/SGD/Grafico/disciplinasAdmin.jsf";
	}
	
	private void atualizarLista() {
		Transaction trns = null;  
		Session session = null;
		session = HibernateUtil.getSession().openSession();		
		trns = session.beginTransaction();
		this.lista = (ArrayList<Disciplina>) session.createQuery("from Disciplina as u order by u.media desc").list();
		int i = 1;
		for(Disciplina disciplina : lista) disciplina.setRank(i++);
	}
	
	public String salvar(){
		System.out.println("Salvando disciplina:" + disciplina.getNome());
		
		Session session = null;
		Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
		
		try {
			trns = session.beginTransaction();
			session.save(disciplina);
			session.getTransaction().commit();

		} catch (RuntimeException e) {

			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
		disciplina = new Disciplina();
		atualizarLista();
		
		FacesContext.getCurrentInstance(). addMessage("confirm:message", new FacesMessage("Disciplina cadastrado com sucesso!"));
		
//		DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
//		disciplinaDAO.adiciona(disciplina);
		
		return "/Cadastro/disciplina.jsf";		
	}
	
	public String carregar(){
		System.out.println("Carregando disciplina:" + disciplina.getNome());		
		return "../Editar/disciplina.xhtml";		
	}

	public String alterar(){
		System.out.println("Alterando disciplina:" + disciplina.getNome());
		
		System.out.println("Alterando disciplina:" + disciplina.getId());
	
		
		Transaction trns = null;  
		Session session = HibernateUtil.getSession().openSession();
								
		try {
			trns = session.beginTransaction();   
		    String hqlUpdate = "update Disciplina u set u.nome = :newName, u.codigo = :newCodigo where u.id = :oldId";
		    int updatedEntities = session.createQuery( hqlUpdate )   
		    		 .setString( "newName", disciplina.getNome() )
		    		 .setString( "newCodigo", disciplina.getCodigo() )
		    		 .setInteger( "oldId", disciplina.getId() )   
		    		 .executeUpdate();    
		     trns.commit();  
		     
		     
		} catch (RuntimeException e){
			
			if(trns != null){    trns.rollback();   }  
		     e.printStackTrace();  } 
		    finally{   session.flush();   session.close();  
		    
		    } 		
		
		
//		DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
//		disciplinaDAO.altera(disciplina);
//		
		return buscarDisciplinas();		
	}

	public String buscarDisciplinas() {
		atualizarLista();				
		return "../Listagem/disciplina.xhtml";
	}	
	
	public Disciplina buscaId(int id){
		
		Session session = null;
		Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
		
		trns = session.beginTransaction();
		Disciplina disciplina;		
		disciplina = (Disciplina)session.createQuery("from Disciplina as u where u.id = :oldId").setInteger("oldId", id).uniqueResult();
		
		return disciplina;		
	}
	
	/****************************************
	 * SETTERS & GETTERS
	 ****************************************/
	
	public void setDisciplina(Disciplina disciplina){this.disciplina = disciplina; };
	public Disciplina getDisciplina(){return this.disciplina; };
	
	public ArrayList<Disciplina> getLista() {return this.lista; }
	
	public List<Disciplina> getFilteredDisciplinas() {
		return filteredDisciplinas;
	}

	public void setFilteredDisciplinas(List<Disciplina> filteredDisciplinas) {
		this.filteredDisciplinas = filteredDisciplinas;
	}

	public ArrayList<String> getSemestres() {
		return semestres;
	}

	public void setSemestres(ArrayList<String> semestres) {
		this.semestres = semestres;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public String getOrdem() {
		return ordem;
	}

	public void setOrdem(String ordem) {
		this.ordem = ordem;
	}

	public ArrayList<String> getOrdens() {
		return ordens;
	}

	public void setLista(ArrayList<Disciplina> lista) {
		this.lista = lista;
	}
	

}