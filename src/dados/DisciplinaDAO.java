package dados;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Disciplina;

public class DisciplinaDAO {
	public boolean adiciona(Disciplina d) {
		ResultSet rs;
		String comandoSQL_Nome = "SELECT * FROM disciplina WHERE nome like  '"
				+ d.getNome() + "'";
		String cmdSQL = "INSERT INTO disciplina(codigo,nome,media)VALUES('"
				+ d.getCodigo() + "','" + d.getNome() + "','" + d.getMedia()
				+ "')";
		System.out.println(cmdSQL);

		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL_Nome);
			System.out.println(rs.first());
			if (rs.first() == false) {
				stmt.executeUpdate(cmdSQL);
				stmt.close();
				// JOptionPane.showMessageDialog(null,
				// "Cadastro efetuado com sucesso!", "Confirma��o do Sistema",
				// JOptionPane.INFORMATION_MESSAGE);
			} else {
				// JOptionPane.showMessageDialog(null,
				// "Falha ao cadastrar. Matricula j� cadastrada no sistema!",
				// "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);

			}
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			// JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
			e.getStackTrace();
			return false;
		}
		return true;
	}

	public boolean altera(Disciplina d) {
		if (d != null) {
			String comandoSQL = "UPDATE disciplina SET codigo='" + d.getCodigo() + "', nome='" + d.getNome() 
					+ "', media='"+ d.getMedia() +"' WHERE id = " + d.getId();
			System.out.println(comandoSQL);
			try {
				java.sql.Statement stmt = (Statement) Conexao.getStatement();
				stmt.executeUpdate(comandoSQL);
				// JOptionPane.showMessageDialog(null,
				// "Altera��o de cadastro efetuada com sucesso!",
				// "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
				stmt.close();
			} catch (SQLException e) {
				// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
				e.getStackTrace();
				return false;
			}
		} else {
			// throw new MedicoErro();
		}
		return true;
	}

	/**
	 * A pesquisa tem como parâmetro um nome e retorna uma lista de disciplinas
	 * que tem a substring "nome" em seu nome
	 * 
	 * @param nome
	 *            - Substring buscada no banco de dados
	 * @return - Retorna uma lista de disciplinas
	 */
	public List busca(String nome) {
		ResultSet rs;
		List disciplinas = new ArrayList<Disciplina>();
		String comandoSQL;
		comandoSQL = "SELECT * FROM disciplina WHERE nome LIKE'%" + nome + "%'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next()) {
				Disciplina disciplina = new Disciplina(); // rs.getInt("id"),rs.getString("nome")
				disciplina.setId(rs.getInt("id"));
				disciplina.setCodigo(rs.getString("codigo"));
				disciplina.setNome(rs.getString("nome"));
				disciplina.setMedia(rs.getFloat("media"));
				disciplinas.add(disciplina);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return disciplinas;
	}

	/**
	 * Retorna uma única disciplina. Pode ser utilizada para eliminar a
	 * ambiguidade da lista gerada pelo método "getDisciplinas(String nome)".
	 * 
	 * @param id
	 *            - Identificador exclusivo da disciplina
	 * @return - Retorna uma única disciplina buscada no banco de dados pelo seu
	 *         id
	 */
	public Disciplina busca(int id) {
		ResultSet rs;
		String comandoSQL;
		comandoSQL = "SELECT * FROM disciplina WHERE id LIKE " + id;
		System.out.println(comandoSQL);
		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			rs.next();
			Disciplina disciplina = new Disciplina(); // rs.getInt("crm"),rs.getString("nome")
			disciplina.setId(rs.getInt("id"));
			disciplina.setCodigo(rs.getString("codigo"));
			disciplina.setNome(rs.getString("nome"));
			disciplina.setMedia(rs.getFloat("media"));
			stmt.close();
			return disciplina;

		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null, "Erro no pesquisar por id!");
			e.printStackTrace();
			System.out.println(e);
		}

		return null;
	}

	/**
	 * Retorna uma lista em ordem alfabética com todas as disciplinas
	 * cadastradas no banco de dados
	 * 
	 * @return
	 */
	public ArrayList<Disciplina> lista() {
		ResultSet rs;
		ArrayList<Disciplina> disciplinas = new ArrayList();
		String comandoSQL;
		comandoSQL = "SELECT * FROM disciplina ORDER BY nome";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next()) {
				Disciplina disciplina = new Disciplina();
				disciplina.setId(rs.getInt("id"));
				disciplina.setCodigo(rs.getString("codigo"));
				disciplina.setNome(rs.getString("nome"));
				disciplina.setMedia(rs.getFloat("media"));
				disciplinas.add(disciplina);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println(e);
			e.printStackTrace();
		}
		return disciplinas;
	}
}