package dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Aluno;
import model.MediaAluno;
import model.MediaCurso;
import model.MediaProfessor;

public class MediaCursoDAO {
	public boolean adiciona(MediaCurso m){
	    ResultSet rs;
	    String comandoSQL_Nome ="SELECT * FROM media_semestre_curso WHERE curso_id='"
	    		+m.getCurso().getId()+ "' AND semestre='"+m.getSemestre()+"'";
	    String cmdSQL = "INSERT INTO media_semestre_curso(curso_id,semestre,media)VALUES('"
	    				+m.getCurso().getId()+"','"
	    				+m.getSemestre()+"','"
	    				+m.getMedia()+"')";
	    System.out.println(cmdSQL);
	    
	    try {
	        Statement stmt = Conexao.getStatement();
	        rs = stmt.executeQuery(comandoSQL_Nome);
	        System.out.println(rs.first());
	        if(rs.first() == false){
	            stmt.executeUpdate(cmdSQL);
	            stmt.close();
	         //   JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
	        }else{
	         //   JOptionPane.showMessageDialog(null, "Falha ao cadastrar. Matricula j� cadastrada no sistema!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
	        	altera(m);
	        }
	        stmt.close();
	    } catch (SQLException e) {
	    	System.out.println(e.toString());
	        //JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
	        e.getStackTrace();
	        return false;
	    }
	    return true;
	}
	
	private boolean altera(MediaCurso m){
        if(m!=null) {
            String comandoSQL = "UPDATE media_semestre_curso SET media='"+m.getMedia()
            		+ "' WHERE curso_id = "+m.getCurso().getId() + " AND semestre = "
            		+ m.getSemestre();
            System.out.println(comandoSQL);
            try {	
                java.sql.Statement stmt = (Statement) Conexao.getStatement();
                stmt.executeUpdate(comandoSQL);
                //JOptionPane.showMessageDialog(null, "Altera��o de cadastro efetuada com sucesso!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
                stmt.close();
            } catch (SQLException e) {
                //JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
                e.getStackTrace();
                return false;
            }
        } else {
                //throw new MedicoErro();
        }
        return true;
    }
	
	public ArrayList<MediaCurso> lista(){
        ResultSet rs;
        ArrayList<MediaCurso> mc = new ArrayList<MediaCurso>();
		String comandoSQL = "SELECT * FROM media_semestre_curso";
        System.out.println(comandoSQL);
        try {
            java.sql.Statement stmt = (Statement) Conexao.getStatement();
            rs = stmt.executeQuery(comandoSQL);
            CursoDAO cd = new CursoDAO();
	 		while(rs.next()) {            
                MediaCurso m = new MediaCurso(); 
                m.setId(rs.getInt("id"));
                m.setCurso(cd.busca(rs.getInt("curso_id")));
                m.setSemestre(rs.getString("semestre"));
                m.setMedia(rs.getFloat("media"));
                mc.add(m);
            }
            stmt.close();

            stmt.close();
        } catch (SQLException e) {
            //JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
            e.getStackTrace();
            return null;
        }
        return mc;
    }
	
	public ArrayList<MediaCurso> lista(String semestre){
        ResultSet rs;
        ArrayList<MediaCurso> mc = new ArrayList<MediaCurso>();
		String comandoSQL = "SELECT * FROM media_semestre_curso WHERE semestre LIKE '"+semestre+"' ORDER BY media DESC";
        System.out.println(comandoSQL);
        try {
            java.sql.Statement stmt = (Statement) Conexao.getStatement();
            rs = stmt.executeQuery(comandoSQL);
            CursoDAO cd = new CursoDAO();
	 		while(rs.next()) {            
                MediaCurso m = new MediaCurso(); 
                m.setId(rs.getInt("id"));
                m.setCurso(cd.busca(rs.getInt("curso_id")));
                m.setSemestre(rs.getString("semestre"));
                m.setMedia(rs.getFloat("media"));
                mc.add(m);
            }
            stmt.close();

            stmt.close();
        } catch (SQLException e) {
            //JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
            e.getStackTrace();
            return null;
        }
        return mc;
    }	
}
