package dados;

import java.util.ArrayList;

import model.Aluno;
import model.Curso;
import model.Disciplina;
import model.MediaCurso;
import model.MediaDisciplina;
import model.MediaProfessor;
import model.Pessoa;
import model.TipoPessoa;
import model.Turma;
import model.Universidade;

public class Calculo {
	
	private float calcularMedia(ArrayList<Float> medias) {
		float media = 0;
		int i;
		for(i=0;i<medias.size();i++)
			media+=medias.get(i);
		if(i>0)
			return media/i;
		return media = 0;
	}
	
	public void calcularMedia(Aluno aluno) {
		MediaAlunoDAO md = new MediaAlunoDAO();
		ArrayList<Float> medias = md.buscaMedias(aluno);
		aluno.setMedia(calcularMedia(medias));
		new PessoaDAO().altera(aluno);
	}
	
	public void calcularMedia(Turma turma) {
		MediaAlunoDAO md = new MediaAlunoDAO();
		ArrayList<Float> medias = md.buscaMedias(turma);
		turma.setMedia(calcularMedia(medias));
		new TurmaDAO().altera(turma);
	}
	
	/**
	 * Este método calcula a média de um professor em relação à uma determinada disciplina passada como parâmetro
	 */
	public void calcularMedia(Pessoa professor, Disciplina disciplina) {
		TurmaDAO td = new TurmaDAO();
		ArrayList<Float> medias = td.buscaMedias(professor, disciplina);
		MediaProfessor mp = new MediaProfessor();
		mp.setDisciplina(disciplina);
		mp.setProfessor(professor);
		mp.setMedia(calcularMedia(medias));
		new MediaProfessorDAO().adiciona(mp);
	}
	
	public void calcularMedia(Disciplina disciplina) {
		TurmaDAO td = new TurmaDAO();
		ArrayList<Float> medias = td.buscaMedias(disciplina);
		disciplina.setMedia(calcularMedia(medias));
		new DisciplinaDAO().altera(disciplina);
	}
	
	public void calcularMedia(Curso curso) {		
		PessoaDAO pd = new PessoaDAO();
		ArrayList<Float> medias = pd.buscaMedias(curso);		
		curso.setMedia(calcularMedia(medias));
		new CursoDAO().altera(curso);
	}
	
	public void calcularMedia(Universidade universidade) {
		CursoDAO cd = new CursoDAO();
		ArrayList<Float> medias = cd.buscaMedias();
		universidade.setMedia(calcularMedia(medias));
		new UniversidadeDAO().altera(universidade);
	}
	
	/**
	 * Este método cácula a média de um curso para um determinado semestre passado como parâmetro
	 */
	public void calcularMedia(Curso curso, String semestre) {
		MediaAlunoDAO md = new MediaAlunoDAO();
		ArrayList<Float> medias = md.buscaMedias(curso, semestre);
		MediaCurso mc = new MediaCurso();
		mc.setCurso(curso);
		mc.setSemestre(semestre);
		mc.setMedia(calcularMedia(medias));
		new MediaCursoDAO().adiciona(mc);
	}
	
	/**
	 * Este método cácula a média de uma disciplina para um determinado semestre passado como parâmetro
	 */
	public void calcularMedia(Disciplina disciplina, String semestre) {
		TurmaDAO td = new TurmaDAO();
		ArrayList<Float> medias = td.buscaMedias(disciplina, semestre);
		MediaDisciplina md = new MediaDisciplina();
		md.setDisciplina(disciplina);
		md.setSemestre(semestre);
		md.setMedia(calcularMedia(medias));
		new MediaDisciplinaDAO().adiciona(md);
	}
			
	/*
	 * Eliminar este método após finalização dos testes
	 */
	public static void main(String args[]) {
		
		Calculo c = new Calculo();
		
		PessoaDAO pd = new PessoaDAO();
		Aluno a = (Aluno) pd.busca(4); 
		c.calcularMedia(a);
 				
		TurmaDAO td = new TurmaDAO();
		Turma turma = td.busca(4);
		c.calcularMedia(turma);
		
		Pessoa p = pd.busca(3);
		Disciplina d = new DisciplinaDAO().busca(2);
		c.calcularMedia(p, d);
		
		DisciplinaDAO dd = new DisciplinaDAO();
		Disciplina disciplina = dd.busca(2);
		c.calcularMedia(disciplina);
		c.calcularMedia(disciplina, "2013.1");

		CursoDAO cd = new CursoDAO();
		Curso curso = cd.busca(1);
		c.calcularMedia(curso);
		c.calcularMedia(curso, "2013.1");

		UniversidadeDAO ud = new UniversidadeDAO();
		Universidade universidade = ud.busca(1);
		c.calcularMedia(universidade);
		

	}
}
