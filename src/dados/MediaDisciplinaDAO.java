package dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.MediaCurso;
import model.MediaDisciplina;

public class MediaDisciplinaDAO {
	public boolean adiciona(MediaDisciplina m) {
		ResultSet rs;
		String comandoSQL_Nome = "SELECT * FROM media_semestre_disciplina WHERE disciplina_id='"
				+ m.getDisciplina().getId()
				+ "' AND semestre='"
				+ m.getSemestre() + "'";
		String cmdSQL = "INSERT INTO media_semestre_disciplina(disciplina_id,semestre,media)VALUES('"
				+ m.getDisciplina().getId()
				+ "','"
				+ m.getSemestre()
				+ "','"
				+ m.getMedia() + "')";
		System.out.println(cmdSQL);

		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL_Nome);
			System.out.println(rs.first());
			if (rs.first() == false) {
				stmt.executeUpdate(cmdSQL);
				stmt.close();
				// JOptionPane.showMessageDialog(null,
				// "Cadastro efetuado com sucesso!",
				// "Confirma��o do Sistema",
				// JOptionPane.INFORMATION_MESSAGE);
			} else {
				// JOptionPane.showMessageDialog(null,
				// "Falha ao cadastrar. Matricula j� cadastrada no sistema!",
				// "Confirma��o do Sistema",
				// JOptionPane.INFORMATION_MESSAGE);
				altera(m);
			}
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			// JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
			e.getStackTrace();
			return false;
		}
		return true;
	}

	private boolean altera(MediaDisciplina m) {
		if (m != null) {
			String comandoSQL = "UPDATE media_semestre_disciplina SET media='"
					+ m.getMedia() + "' WHERE disciplina_id = "
					+ m.getDisciplina().getId() + " AND semestre = "
					+ m.getSemestre();
			System.out.println(comandoSQL);
			try {
				java.sql.Statement stmt = (Statement) Conexao.getStatement();
				stmt.executeUpdate(comandoSQL);
				// JOptionPane.showMessageDialog(null,
				// "Altera��o de cadastro efetuada com sucesso!",
				// "Confirma��o do Sistema",
				// JOptionPane.INFORMATION_MESSAGE);
				stmt.close();
			} catch (SQLException e) {
				// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
				e.getStackTrace();
				return false;
			}
		} else {
			// throw new MedicoErro();
		}
		return true;
	}

	public ArrayList<MediaDisciplina> lista() {
		ResultSet rs;
		ArrayList<MediaDisciplina> md = new ArrayList<MediaDisciplina>();
		String comandoSQL = "SELECT * FROM media_semestre_disciplina";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = (Statement) Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			DisciplinaDAO dd = new DisciplinaDAO();
			while (rs.next()) {
				MediaDisciplina m = new MediaDisciplina();
				m.setId(rs.getInt("id"));
				m.setDisciplina(dd.busca(rs.getInt("disciplina_id")));
				m.setSemestre(rs.getString("semestre"));
				m.setMedia(rs.getFloat("media"));
				md.add(m);
			}
			stmt.close();

			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
			e.getStackTrace();
			return null;
		}
		return md;
	}

	public ArrayList<MediaDisciplina> lista(String semestre) {
		ResultSet rs;
		ArrayList<MediaDisciplina> md = new ArrayList<MediaDisciplina>();
		String comandoSQL = "SELECT * FROM media_semestre_disciplina WHERE semestre LIKE '"+semestre+"'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = (Statement) Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			DisciplinaDAO dd = new DisciplinaDAO();
			while (rs.next()) {
				MediaDisciplina m = new MediaDisciplina();
				m.setId(rs.getInt("id"));
				m.setDisciplina(dd.busca(rs.getInt("disciplina_id")));
				m.setSemestre(rs.getString("semestre"));
				m.setMedia(rs.getFloat("media"));
				md.add(m);
			}
			stmt.close();

			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
			e.getStackTrace();
			return null;
		}
		return md;
	}
}
