package dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Aluno;
import model.Curso;
import model.Disciplina;
import model.MediaAluno;
import model.Pessoa;
import model.TipoPessoa;
import model.Turma;

public class TurmaDAO {

	public boolean adiciona(Turma t) {
		ResultSet rs;
		String comandoSQL_Nome = "SELECT * FROM turma WHERE codigo like  '"
				+ t.getCodigo() + "' AND semestre like '" + t.getSemestre()
				+ "' AND disciplina_id = '" + t.getDisciplina().getId() + "' ";
		String cmdSQL = "INSERT INTO turma(semestre,codigo,disciplina_id,pessoa_id,media) VALUES('"
				+ t.getSemestre() + "','"
				+ t.getCodigo() + "','"
				+ t.getDisciplina().getId() + "','"
				+ t.getProfessor().getId() + "','" 
				+ t.getMedia() + "')";
		System.out.println(cmdSQL);

		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL_Nome);
			System.out.println(rs.first());
			if (rs.first() == false) {
				stmt.executeUpdate(cmdSQL);
				stmt.close();
				return true;
				// JOptionPane.showMessageDialog(null,
				// "Cadastro efetuado com sucesso!", "Confirma��o do Sistema",
				// JOptionPane.INFORMATION_MESSAGE);
			} else {
				// JOptionPane.showMessageDialog(null,
				// "Falha ao cadastrar. Matricula j� cadastrada no sistema!",
				// "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);

			}
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			// JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
			e.getStackTrace();
			return false;
		}
		return true;
	}

	public boolean altera(Turma t) {
		if (t != null) {
			String comandoSQL = "UPDATE turma SET media='" + t.getMedia() + "', semestre='"+t.getSemestre()
					+"', codigo='"+t.getCodigo()+"', disciplina_id='"+t.getDisciplina().getId()
					+"', pessoa_id='"+t.getProfessor().getId()+"' WHERE id = " + t.getId();
			System.out.println(comandoSQL);
			try {
				java.sql.Statement stmt = (Statement) Conexao.getStatement();
				stmt.executeUpdate(comandoSQL);
				stmt.close();
				MediaAlunoDAO mad = new MediaAlunoDAO();
				mad.altera(t);
				return true;
			} catch (SQLException e) {
				// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
				e.getStackTrace();
				System.out.println(e);
			}
		} else {
			// throw new MedicoErro();
		}
		return false;
	}

	public Turma busca(int id) {
		ResultSet rs;
		String comandoSQL;
		comandoSQL = "select * from turma where id ='" + id + "'";
		System.out.println(comandoSQL);
		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			rs.next();
			Turma m = new Turma();
			Float media = rs.getFloat("media");
			m.setId(rs.getInt("id"));
			m.setCodigo(rs.getInt("codigo"));
			m.setMedia(media);
			m.setSemestre(rs.getString("semestre"));

			Disciplina disciplina = new DisciplinaDAO().busca(rs.getInt("disciplina_id"));
			Pessoa pessoa = new PessoaDAO().busca(rs.getInt("pessoa_id"));
			//ArrayList<MediaAluno> mediasAlunos = new MediaAlunoDAO().buscaAlunos(id);
			m.setDisciplina(disciplina);
			m.setProfessor(pessoa);
			//m.setMediasAlunos(mediasAlunos);

			stmt.close();

			return m;

		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null, "Erro no pesquisar por id!");
			e.printStackTrace();
			System.out.println(e);
		}

		return null;
	}

	public ArrayList<Turma> lista() {
		ResultSet rs;
		ArrayList<Turma> turmas = new ArrayList<Turma>();
		String comandoSQL;
		comandoSQL = "select * from turma";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
			PessoaDAO pessoaDAO = new PessoaDAO();
			//MediaAlunoDAO mediaAlunoDAO = new MediaAlunoDAO();
			while (rs.next()) {
				Turma t = new Turma();
				Float media = rs.getFloat("media");
				t.setId(rs.getInt("id"));
				t.setCodigo(rs.getInt("codigo"));
				t.setMedia(media);
				t.setSemestre(rs.getString("semestre"));

				Disciplina disciplina = disciplinaDAO.busca(rs.getInt("disciplina_id"));
				Pessoa pessoa = pessoaDAO.busca(rs.getInt("pessoa_id"));
				//ArrayList<MediaAluno> mediasAlunos = mediaAlunoDAO.buscaAlunos(t.getId());
				t.setDisciplina(disciplina);
				t.setProfessor(pessoa);
				//t.setMediasAlunos(mediasAlunos);

				turmas.add(t);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return turmas;
	}
	
	public ArrayList<Turma> lista(String semestre) {
		ResultSet rs;
		ArrayList<Turma> turmas = new ArrayList<Turma>();
		String comandoSQL;
		comandoSQL = "select * from turma where semestre like '" + semestre + "'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
			PessoaDAO pessoaDAO = new PessoaDAO();
			//MediaAlunoDAO mediaAlunoDAO = new MediaAlunoDAO();
			while (rs.next()) {
				Turma t = new Turma();
				Float media = rs.getFloat("media");
				t.setId(rs.getInt("id"));
				t.setCodigo(rs.getInt("codigo"));
				t.setMedia(media);
				t.setSemestre(rs.getString("semestre"));

				Disciplina disciplina = disciplinaDAO.busca(rs.getInt("disciplina_id"));
				Pessoa pessoa = pessoaDAO.busca(rs.getInt("pessoa_id"));
				//ArrayList<MediaAluno> mediasAlunos = mediaAlunoDAO.buscaAlunos(t.getId());
				t.setDisciplina(disciplina);
				t.setProfessor(pessoa);
				//t.setMediasAlunos(mediasAlunos);

				turmas.add(t);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return turmas;
	}

	public ArrayList<Turma> lista(Pessoa professor) {
		ResultSet rs;
		ArrayList<Turma> turmas = new ArrayList<Turma>();
		String comandoSQL;
		comandoSQL = "select * from turma where pessoa_id like '" + professor.getId() + "'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
			PessoaDAO pessoaDAO = new PessoaDAO();
			//MediaAlunoDAO mediaAlunoDAO = new MediaAlunoDAO();
			while (rs.next()) {
				Turma t = new Turma();
				Float media = rs.getFloat("media");
				t.setId(rs.getInt("id"));
				t.setCodigo(rs.getInt("codigo"));
				t.setMedia(media);
				t.setSemestre(rs.getString("semestre"));

				Disciplina disciplina = disciplinaDAO.busca(rs.getInt("disciplina_id"));
				Pessoa pessoa = pessoaDAO.busca(rs.getInt("pessoa_id"));
				//ArrayList<MediaAluno> mediasAlunos = mediaAlunoDAO.buscaAlunos(t.getId());
				t.setDisciplina(disciplina);
				t.setProfessor(pessoa);
				//t.setMediasAlunos(mediasAlunos);

				turmas.add(t);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return turmas;
	}

	public ArrayList<Float> buscaMedias(Pessoa professor, Disciplina disciplina) {
		ResultSet rs;
		String comandoSQL;
		ArrayList<Float> medias = new ArrayList<Float>();
		comandoSQL = "SELECT media FROM turma WHERE pessoa_id = '" + professor.getId() 
				+ "' AND disciplina_id = '" + disciplina.getId() + "'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next())
				medias.add(rs.getFloat("media"));
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return medias;
	}

	public ArrayList<Float> buscaMedias(Disciplina disciplina) {
		ResultSet rs;
		String comandoSQL;
		ArrayList<Float> medias = new ArrayList<Float>();
		comandoSQL = "SELECT media FROM turma WHERE disciplina_id = '" + disciplina.getId() + "'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next())
				medias.add(rs.getFloat("media"));
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return medias;
	}

	public ArrayList<Float> buscaMedias(Disciplina disciplina, String semestre) {
		ResultSet rs;
		String comandoSQL;
		ArrayList<Float> medias = new ArrayList<Float>();
		comandoSQL = "SELECT media FROM turma WHERE disciplina_id = '" + disciplina.getId() 
				+ "' AND semestre = '" + semestre + "'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next())
				medias.add(rs.getFloat("media"));
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return medias;
	}
	
	public boolean excluiAlunoTurma(int idTurma, int idAluno){
		
		ResultSet rs;
		String comandoSQL_Nome = "DELETE FROM media_turma_aluno WHERE aluno_id =  '" + idAluno + "' and turma_id = '" + idTurma + "'";
		System.out.println(comandoSQL_Nome);
		try {
			Statement stmt = Conexao.getStatement();
			stmt.executeUpdate(comandoSQL_Nome);
			stmt.close();
			
		} catch (SQLException e) {
			System.out.println(e.toString());
			
			e.getStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean adicionaAlunoTurma(int idTurma, int idAluno, int idCurso, String semestre, float media){
		
			ResultSet rs;
			String comandoSQL_Nome = "SELECT * FROM media_turma_aluno WHERE aluno_id =  '" + idAluno + "' and turma_id = '" + idTurma + "'";
			String cmdSQL = "INSERT INTO media_turma_aluno(turma_id,aluno_id,curso_id,semestre,media) VALUES('"
					+ idTurma + "','"
					+ idAluno + "','"
					+ idCurso + "','"
					+ semestre + "','" 
					+ media + "')";
			System.out.println(cmdSQL);

			try {
				Statement stmt = Conexao.getStatement();
				rs = stmt.executeQuery(comandoSQL_Nome);
				System.out.println(rs.first());
				if (rs.first() == false) {
					stmt.executeUpdate(cmdSQL);
					stmt.close();
					return true;
					
				} else {				

				}
				stmt.close();
			} catch (SQLException e) {
				System.out.println(e.toString());
				
				e.getStackTrace();
				return false;
			}
			return true;
		}
	
	public ArrayList<Aluno> listaAlunos(int idTurma) {
		ResultSet rs;
		ArrayList<Aluno> pessoas = new ArrayList<Aluno>();
		String comandoSQL;
		comandoSQL = "SELECT pessoa.id, pessoa.nome, pessoa.senha, pessoa.tipo, pessoa.curso_id, pessoa.media, pessoa.matricula FROM pessoa INNER JOIN media_turma_aluno ON pessoa.id=media_turma_aluno.aluno_id AND media_turma_aluno.turma_id =" + idTurma ;
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			Aluno p;
			CursoDAO cd = new CursoDAO();
			while (rs.next()) {
				Aluno a = new Aluno();
				a.setCurso(cd.busca(rs.getInt("curso_id")));
				a.setMedia(rs.getFloat("media"));
				
				p = a;
				
				TipoPessoa tipo = TipoPessoa.values()[rs.getInt("tipo")];

				p.setId(rs.getInt("id"));
				p.setNome(rs.getString("nome"));
				p.setMatricula(rs.getString("matricula"));
				p.setSenha(rs.getString("senha"));
				p.setTipo(tipo);
				pessoas.add(p);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return pessoas;
	}
		
	
	
}
