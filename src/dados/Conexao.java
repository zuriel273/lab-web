package dados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Conexao{

    private static Connection connection = null;
    
    public static Statement getStatement() throws SQLException{
        
        if(connection == null){
            String url = "jdbc:mysql://localhost:3306/sgd_db";
            try{
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(url, "root", "");
            } 
            catch(Exception e){
            	try{
                    Class.forName("com.mysql.jdbc.Driver");
                    connection = DriverManager.getConnection(url, "root", "");
                }
            	
            	catch(Exception e2){
            		JOptionPane.showMessageDialog(null, e2.toString() + " FALHA NA CONEXAO");
            	}
            }
        }
        return connection.createStatement();
    }
    
    public Connection getConnection(){  
       try{  
          Class.forName("com.mysql.jdbc.Driver").newInstance();  
          String driver = "jdbc:mysql://localhost:3306/sgd_db";  
          Connection con = DriverManager.getConnection(driver,"root","");   
          return con;  
        }  
        catch(Exception e){  
            e.printStackTrace();  
        }  
        return null;  
    }  
    
}
