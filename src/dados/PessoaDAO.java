package dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import model.Aluno;
import model.Curso;
import model.Pessoa;
import model.TipoPessoa;

public class PessoaDAO {

	public boolean adiciona(Pessoa p) {
		if (p != null) {
			ResultSet rs_matricula;
			String comandoSQL_matricula;
			int idCurso = 0;
			float media = 0;
			
			if (p instanceof Aluno) {
				Aluno a = (Aluno) p;
				idCurso = a.getCurso().getId();
			}
			comandoSQL_matricula = "SELECT * FROM pessoa WHERE matricula like '"
					+ p.getMatricula() + "'";
			String comandoSQL = "INSERT INTO pessoa(nome,matricula,senha,tipo,curso_id,media) "
					+ "VALUES('" + p.getNome() + "','" + p.getMatricula() + "','" + p.getSenha() + "','"
					+ p.getTipo().value() + "','" + idCurso + "','" + media + "')";
			System.out.println(comandoSQL_matricula);

			try {
				java.sql.Statement stmt = Conexao.getStatement();
				rs_matricula = stmt.executeQuery(comandoSQL_matricula);
				System.out.println(rs_matricula.first());
				if (rs_matricula.first() == false) {
					stmt.executeUpdate(comandoSQL);
					stmt.close();
					
					return true;
					
				} else {
					
				}
				stmt.close();
			} catch (SQLException e) {
				// JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
				System.out.println(e.toString());
				e.getStackTrace();
			}
		} else {

		}
		return false;
	}

	public boolean altera(Pessoa p) {
		int idCurso = 0;
		float media = 0;
		if (p instanceof Aluno) {
			Aluno a = (Aluno) p;
			idCurso = a.getCurso().getId();
			media = a.getMedia();
		}
		if (p != null) {
			String comandoSQL = "UPDATE pessoa SET nome='" + p.getNome()
					+ "',matricula='" + p.getMatricula() + "',senha='" + p.getSenha() + "', curso_id='"
					+ idCurso + "', media='" + media + "' WHERE id = " + p.getId();
			System.out.println(comandoSQL);
			try {
				java.sql.Statement stmt = (Statement) Conexao.getStatement();
				stmt.executeUpdate(comandoSQL);
				stmt.close();
				return true;
			} catch (SQLException e) {
				// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
				e.getStackTrace();
				System.out.println(e);
			}
		} else {
			// throw new MedicoErro();
		}
		return false;
	}

	/*
	 * Recebe comando SQL para efetuar a busca e devolver uma lista de pessoas
	 */
	private ArrayList<Pessoa> lista(String cmdSql) {
		ResultSet rs;
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		String comandoSQL;
		comandoSQL = cmdSql;
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			Pessoa p;
			CursoDAO cd = new CursoDAO();
			while (rs.next()) {
				TipoPessoa tipo = TipoPessoa.values()[rs.getInt("tipo")];
				if (tipo == TipoPessoa.ALUNO) {
					Aluno a = new Aluno();
					a.setCurso(cd.busca(rs.getInt("curso_id")));
					a.setMedia(rs.getFloat("media"));
					
					p = a;
				} else
					p = new Pessoa();

				p.setId(rs.getInt("id"));
				p.setNome(rs.getString("nome"));
				p.setMatricula(rs.getString("matricula"));
				p.setSenha(rs.getString("senha"));
				p.setTipo(tipo);
				pessoas.add(p);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return pessoas;
	}
	
	public Pessoa autorizar(String login, String senha){
		ResultSet rs;
		Pessoa pessoa = null;
		String comandoSQL = "select * from pessoa where "
						  + "matricula = '" + login + "' and "
						  + "senha = '" + senha + "'";
		System.out.println(comandoSQL);
		
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			
			CursoDAO cd = new CursoDAO();
			
			if (rs.next()) {
				TipoPessoa tipo = TipoPessoa.getTipoPessoa(rs.getInt("tipo"));
				
				if (tipo == TipoPessoa.ALUNO) {
					pessoa = new Aluno();
					((Aluno) pessoa).setCurso(cd.busca(rs.getInt("curso_id")));
					((Aluno) pessoa).setMedia(rs.getFloat("media"));
				}else{
					pessoa = new Pessoa();
				}
				
				pessoa.setId(rs.getInt("id"));
				pessoa.setNome(rs.getString("nome"));
				pessoa.setMatricula(rs.getString("matricula"));
				pessoa.setSenha(rs.getString("senha"));
				pessoa.setTipo(tipo);				
			}
			
		}catch (SQLException e){
			System.out.println("erro em PessoaDAO.autorizar:");
			e.printStackTrace();
		}
		return pessoa;
	}
	/**
	 * Lista todas as pessoas cadastradas no banco de dados (professores, aluno
	 * e administrador)
	 */
	public ArrayList<Pessoa> lista() {
		String cmdSQL = "select * from pessoa ORDER BY nome";
		return lista(cmdSQL);
	}

	/**
	 * Lista as pessoas cadastradas no banco de dados do tipo informado
	 */
	public ArrayList<Pessoa> lista(TipoPessoa tipo) {
		String cmdSQL = "SELECT * FROM pessoa WHERE tipo LIKE '"
				+ tipo.ordinal() + "'ORDER BY nome";
		return lista(cmdSQL);
	}
	
	public List busca(String nome) {
		String cmdSQL = "select * from pessoa where nome like'%" + nome + "%'";;
		return lista(cmdSQL);
	}

	public Pessoa busca(int id) {
		String cmdSQL = "select * from pessoa where id ='" + id + "'";
		try {
			return lista(cmdSQL).get(0);
		} catch (ArrayIndexOutOfBoundsException e) {
			// JOptionPane.showMessageDialog(null, "Erro no pesquisar por id!");
			e.printStackTrace();
			System.out.println(e);
			return null;
		}
	}
	
	public Pessoa buscaPorMatricula(int matricula) {
		String cmdSQL = "select * from pessoa where matricula like '" + matricula + "'";
		try {
			ArrayList<Pessoa> listaux = lista(cmdSQL);
			if (listaux.size() > 0 )
				return listaux.get(0);
			
			return null;
		} catch (ArrayIndexOutOfBoundsException e) {
			
			e.printStackTrace();
			System.out.println(e);
			return null;
		}
	}
	
	public ArrayList<Float> buscaMedias(Curso curso) {
		ArrayList<Float> medias = new ArrayList<Float>();
		ResultSet rs;
		String cmdSQL = "SELECT media FROM pessoa WHERE curso_id LIKE '"
				+ curso.getId() + "'ORDER BY nome";
		System.out.println(cmdSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(cmdSQL);
			Pessoa p;
			CursoDAO cd = new CursoDAO();
			while (rs.next())
				medias.add(rs.getFloat("media"));
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return medias;
	}


}
