package dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Curso;
import model.Universidade;

public class UniversidadeDAO {
	
	public boolean altera(Universidade universidade) {
		String comandoSQL = "UPDATE universidade SET media='" + universidade.getMedia()
				+ "' WHERE id = " + universidade.getId();
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = (Statement) Conexao.getStatement();
			stmt.executeUpdate(comandoSQL);
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
			e.getStackTrace();
			return false;
		}
		return true;
	}
	
	public Universidade busca(int id) {
		ResultSet rs;
		String comandoSQL;
		comandoSQL = "SELECT * FROM universidade WHERE id LIKE '" + id + "'";
		System.out.println(comandoSQL);
		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			rs.next();
			Universidade u = new Universidade();
			u.setId(rs.getInt("id"));
			u.setNome(rs.getString("nome"));
			u.setMedia(rs.getFloat("media"));
			stmt.close();
			return u;

		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null, "Erro no pesquisar por id!");
			e.printStackTrace();
			System.out.println(e);
		}

		return null;
	}
}
