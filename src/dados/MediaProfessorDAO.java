package dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Disciplina;
import model.MediaProfessor;
import model.Pessoa;

public class MediaProfessorDAO {
	public boolean adiciona(MediaProfessor m){
	    ResultSet rs;
	    String comandoSQL_Nome ="SELECT * FROM media_disciplina_professor WHERE disciplina_id='"
	    		+m.getDisciplina().getId()+ "' AND pessoa_id='"+m.getProfessor().getId()+"'";
	    String cmdSQL = "INSERT INTO media_disciplina_professor(disciplina_id,pessoa_id,media)VALUES('"
	    				+m.getDisciplina().getId()+"','"
	    				+m.getProfessor().getId()+"','"
	    				+m.getMedia()+"')";
	    System.out.println(cmdSQL);
	    
	    try {
	        Statement stmt = Conexao.getStatement();
	        rs = stmt.executeQuery(comandoSQL_Nome);
	        System.out.println(rs.first());
	        if(rs.first() == false){
	            stmt.executeUpdate(cmdSQL);
	            stmt.close();
	         //   JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
	        }else{
	         //   JOptionPane.showMessageDialog(null, "Falha ao cadastrar. Matricula j� cadastrada no sistema!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
	        	altera(m);
	        }
	        stmt.close();
	    } catch (SQLException e) {
	    	System.out.println(e.toString());
	        //JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
	        e.getStackTrace();
	        return false;
	    }
	    return true;
	}
	
	private boolean altera(MediaProfessor m){
        if(m!=null) {
            String comandoSQL = "UPDATE media_disciplina_professor SET media='"+m.getMedia()
            		+ "' WHERE disciplina_id = "+m.getDisciplina().getId() + " AND pessoa_id = "
            		+ m.getProfessor().getId();
            System.out.println(comandoSQL);
            try {	
                java.sql.Statement stmt = (Statement) Conexao.getStatement();
                stmt.executeUpdate(comandoSQL);
                //JOptionPane.showMessageDialog(null, "Altera��o de cadastro efetuada com sucesso!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
                stmt.close();
            } catch (SQLException e) {
                //JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
                e.getStackTrace();
                return false;
            }
        } else {
                //throw new MedicoErro();
        }
        return true;
    }
	
    public ArrayList<MediaProfessor> buscaProfessores(Disciplina disciplina){
        ResultSet rs;
	 	String comandoSQL;
	 	ArrayList<MediaProfessor> mediasProfessores = new ArrayList<MediaProfessor>();
	 	comandoSQL = "SELECT * FROM media_disciplina_professor WHERE disciplina_id LIKE "+disciplina.getId();
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
	 		while(rs.next()) {
	 			 MediaProfessor m = new MediaProfessor(); 
                 m.setId(rs.getInt("id"));
                 m.setDisciplina(disciplinaDAO.busca(rs.getInt("disciplina_id")));
                 m.setProfessor((pessoaDAO.buscaPorMatricula(rs.getInt("pessoa_id"))));
                 m.setMedia(rs.getFloat("media"));
                 mediasProfessores.add(m);
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return mediasProfessores;
 	}
	
    public MediaProfessor busca(int idDisciplina, int idProfessor){
        ResultSet rs;
	 	String comandoSQL;
	 	MediaProfessor mediaProfessor = null;
	 	comandoSQL = "SELECT * FROM media_disciplina_professor WHERE disciplina_id LIKE '"+ idDisciplina 
	 			+ "' AND pessoa_id LIKE '"+idProfessor+"'";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		while(rs.first()) {            
                 mediaProfessor = new MediaProfessor(); 
                 mediaProfessor.setId(rs.getInt("id"));
                 mediaProfessor.setDisciplina(disciplinaDAO.busca(rs.getInt("disciplina_id")));
                 mediaProfessor.setProfessor(pessoaDAO.busca(rs.getInt("pessoa_id")));
                 mediaProfessor.setMedia(rs.getFloat("media"));
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return mediaProfessor;
 	}
	
    public ArrayList<Float> buscaMedias(Pessoa professor){
        ResultSet rs;
	 	String comandoSQL;
	 	ArrayList<Float> medias = new ArrayList<Float>();
	 	comandoSQL = "SELECT media FROM media_disciplina_professor WHERE pessoa_id LIKE "+professor.getId();
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		while(rs.next()) {
                 medias.add(rs.getFloat("media"));
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}	
	
    public ArrayList<MediaProfessor> lista(){
        ResultSet rs;
        ArrayList<MediaProfessor> medias = new ArrayList<MediaProfessor>();
	 	String comandoSQL;
	 	comandoSQL = "SELECT * FROM media_disciplina_professor ORDER BY disciplina_id";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		while(rs.next()) {            
                 MediaProfessor mediaProfessor = new MediaProfessor(); 
                 mediaProfessor.setId(rs.getInt("id"));
                 mediaProfessor.setDisciplina(disciplinaDAO.busca(rs.getInt("disciplina_id")));
                 mediaProfessor.setProfessor(pessoaDAO.buscaPorMatricula(rs.getInt("pessoa_id")));
                 mediaProfessor.setMedia(rs.getFloat("media"));
                 medias.add(mediaProfessor);
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}
    
    public ArrayList<MediaProfessor> lista(Pessoa professor){
        ResultSet rs;
        ArrayList<MediaProfessor> medias = new ArrayList<MediaProfessor>();
	 	String comandoSQL;
	 	comandoSQL = "SELECT * FROM media_disciplina_professor WHERE pessoa_id LIKE '"+professor.getId()+"'";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		while(rs.next()) {            
                 MediaProfessor mediaProfessor = new MediaProfessor(); 
                 mediaProfessor.setId(rs.getInt("id"));
                 mediaProfessor.setDisciplina(disciplinaDAO.busca(rs.getInt("disciplina_id")));
                 mediaProfessor.setProfessor(pessoaDAO.buscaPorMatricula(rs.getInt("pessoa_id")));
                 mediaProfessor.setMedia(rs.getFloat("media"));
                 medias.add(mediaProfessor);
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}    

}
