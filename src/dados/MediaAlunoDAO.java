package dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Aluno;
import model.Curso;
import model.MediaAluno;
import model.Turma;

public class MediaAlunoDAO {
	public boolean adiciona(MediaAluno m){
	    ResultSet rs;
	    String comandoSQL_Nome ="SELECT * FROM media_turma_aluno WHERE turma_id='"+m.getTurma().getId()+
	    		"' AND aluno_id='"+m.getAluno().getId()+"'";
	    String cmdSQL = "INSERT INTO media_turma_aluno(turma_id,aluno_id,curso_id,semestre,media)VALUES('"
	    				+m.getTurma().getId()+"','"
	    				+m.getAluno().getId()+"','"
	    				+m.getAluno().getCurso().getId()+"','"
	    				+m.getTurma().getSemestre()+"','"
	    				+m.getMedia()+"')";
	    System.out.println(cmdSQL);
	    
	    try {
	        Statement stmt = Conexao.getStatement();
	        rs = stmt.executeQuery(comandoSQL_Nome);
	        System.out.println(rs.first());
	        if(rs.first() == false){
	            stmt.executeUpdate(cmdSQL);
	            stmt.close();
	         //   JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
	        }else{
	         //   JOptionPane.showMessageDialog(null, "Falha ao cadastrar. Matricula j� cadastrada no sistema!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
	        	
	        }
	        stmt.close();
	    } catch (SQLException e) {
	    	System.out.println(e.toString());
	        //JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
	        e.getStackTrace();
	        return false;
	    }
	    return true;
	}
	
	public boolean altera(MediaAluno m){
        if(m!=null) {
            String comandoSQL = "UPDATE media_turma_aluno SET media='"+m.getMedia()+"' WHERE id = "+m.getId();
            System.out.println(comandoSQL);
            try {	
                java.sql.Statement stmt = (Statement) Conexao.getStatement();
                stmt.executeUpdate(comandoSQL);
                //JOptionPane.showMessageDialog(null, "Altera��o de cadastro efetuada com sucesso!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
                stmt.close();
            } catch (SQLException e) {
                //JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
                e.getStackTrace();
                return false;
            }
        } else {
                //throw new MedicoErro();
        }
        return true;
    }
	
	public boolean altera(Turma turma){
        if(turma!=null) {
            String comandoSQL = "UPDATE media_turma_aluno SET semestre ='"+turma.getSemestre()+"' WHERE id = "+turma.getId();
            System.out.println(comandoSQL);
            try {	
                java.sql.Statement stmt = (Statement) Conexao.getStatement();
                stmt.executeUpdate(comandoSQL);
                //JOptionPane.showMessageDialog(null, "Altera��o de cadastro efetuada com sucesso!", "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
                stmt.close();
            } catch (SQLException e) {
                //JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
                e.getStackTrace();
                return false;
            }
        } else {
                //throw new MedicoErro();
        }
        return true;
    }
	
    public ArrayList<MediaAluno> buscaAlunos(int idTurma){
        ResultSet rs;
	 	String comandoSQL;
	 	ArrayList<MediaAluno> mediasAlunos = new ArrayList<MediaAluno>();
	 	comandoSQL = "SELECT * FROM media_turma_aluno WHERE turma_id LIKE "+idTurma;
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		TurmaDAO turmaDAO = new TurmaDAO();
	 		while(rs.next()) {
	 			 MediaAluno m = new MediaAluno(); 
                 m.setId(rs.getInt("id"));
                 m.setTurma(turmaDAO.busca(rs.getInt("turma_id")));
                 System.out.println("PASSOU AQUI");
                 m.setAluno((Aluno)pessoaDAO.busca(rs.getInt("aluno_id")));
                 m.setMedia(rs.getFloat("media"));
                 mediasAlunos.add(m);
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return mediasAlunos;
 	}
	
    public MediaAluno busca(int idTurma, int idAluno){
        ResultSet rs;
	 	String comandoSQL;
	 	MediaAluno mediaAluno = null;
	 	comandoSQL = "SELECT * FROM media_turma_aluno WHERE turma_id LIKE '"+idTurma+"' AND aluno_id LIKE '"+idAluno+"'";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		TurmaDAO turmaDAO = new TurmaDAO();
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		while(rs.first()) {            
                 mediaAluno = new MediaAluno(); 
                 mediaAluno.setId(rs.getInt("id"));
                 mediaAluno.setTurma(turmaDAO.busca(rs.getInt("turma_id")));
                 mediaAluno.setAluno((Aluno)pessoaDAO.buscaPorMatricula(rs.getInt("aluno_id")));
                 mediaAluno.setMedia(rs.getFloat("media"));
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return mediaAluno;
 	}
	
    public ArrayList<Float> buscaMedias(Aluno aluno){
        ResultSet rs;
	 	String comandoSQL;
	 	ArrayList<Float> medias = new ArrayList<Float>();
	 	comandoSQL = "SELECT media FROM media_turma_aluno WHERE aluno_id LIKE "+aluno.getId();
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		while(rs.next()) {
                 medias.add(rs.getFloat("media"));
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}
	
    public ArrayList<Float> buscaMedias(Turma turma){
        ResultSet rs;
	 	String comandoSQL;
	 	ArrayList<Float> medias = new ArrayList<Float>();
	 	comandoSQL = "SELECT media FROM media_turma_aluno WHERE turma_id LIKE "+turma.getId();
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		while(rs.next()) {
                 medias.add(rs.getFloat("media"));
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}
    
    public ArrayList<Float> buscaMedias(Curso curso, String semestre){
    	ResultSet rs;
	 	String comandoSQL;
	 	ArrayList<Float> medias = new ArrayList<Float>();
	 	comandoSQL = "SELECT media FROM media_turma_aluno WHERE curso_id =" + curso.getId()
	 			+ " AND semestre = '" + semestre + "'";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		while(rs.next())
                 medias.add(rs.getFloat("media"));
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}
	
    public ArrayList<MediaAluno> lista(){
        ResultSet rs;
        ArrayList<MediaAluno> medias = new ArrayList<MediaAluno>();
	 	String comandoSQL;
	 	comandoSQL = "SELECT * FROM media_turma_aluno ORDER BY turma_id";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		TurmaDAO turmaDAO = new TurmaDAO();
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		while(rs.next()) {            
                 MediaAluno mediaAluno = new MediaAluno(); 
                 mediaAluno.setId(rs.getInt("id"));
                 mediaAluno.setTurma(turmaDAO.busca(rs.getInt("turma_id")));
                 mediaAluno.setAluno((Aluno)pessoaDAO.busca(rs.getInt("aluno_id")));
                 mediaAluno.setMedia(rs.getFloat("media"));
                 medias.add(mediaAluno);
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}
    
    public ArrayList<MediaAluno> lista(Aluno aluno){
        ResultSet rs;
        ArrayList<MediaAluno> medias = new ArrayList<MediaAluno>();
	 	String comandoSQL;
	 	comandoSQL = "SELECT * FROM media_turma_aluno WHERE aluno_id LIKE '"+aluno.getId()+"'";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		TurmaDAO turmaDAO = new TurmaDAO();
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		while(rs.next()) {            
                 MediaAluno mediaAluno = new MediaAluno(); 
                 mediaAluno.setId(rs.getInt("id"));
                 mediaAluno.setTurma(turmaDAO.busca(rs.getInt("turma_id")));
                 mediaAluno.setAluno((Aluno)pessoaDAO.busca(rs.getInt("aluno_id")));
                 mediaAluno.setMedia(rs.getFloat("media"));
                 medias.add(mediaAluno);
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}
    
    public ArrayList<MediaAluno> lista(Aluno aluno, String semestre){
        ResultSet rs;
        ArrayList<MediaAluno> medias = new ArrayList<MediaAluno>();
	 	String comandoSQL;
	 	comandoSQL = "SELECT * FROM media_turma_aluno WHERE aluno_id LIKE '"+aluno.getId()+"' AND semestre LIKE '"+semestre+"'";
	 	System.out.println(comandoSQL);
	    try {
	 		java.sql.Statement stmt = Conexao.getStatement();
	 		rs = stmt.executeQuery(comandoSQL);
	 		TurmaDAO turmaDAO = new TurmaDAO();
	 		PessoaDAO pessoaDAO = new PessoaDAO();
	 		while(rs.next()) {            
                 MediaAluno mediaAluno = new MediaAluno(); 
                 mediaAluno.setId(rs.getInt("id"));
                 mediaAluno.setTurma(turmaDAO.busca(rs.getInt("turma_id")));
                 mediaAluno.setAluno((Aluno)pessoaDAO.busca(rs.getInt("aluno_id")));
                 mediaAluno.setMedia(rs.getFloat("media"));
                 medias.add(mediaAluno);
             }
             stmt.close();
	 	} catch (SQLException e) {
			//JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
	    }
        return medias;
 	}   
}
