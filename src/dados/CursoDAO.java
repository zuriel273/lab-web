package dados;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Curso;

public class CursoDAO {
	public boolean adiciona(Curso c) {
		ResultSet rs;
		String comandoSQL_Nome = "SELECT * FROM curso WHERE nome like  '"
				+ c.getNome() + "'";
		String cmdSQL = "INSERT INTO curso(nome,media) VALUES('" + c.getNome()
				+ "','" + c.getMedia() + "')";
		System.out.println(cmdSQL);

		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL_Nome);
			System.out.println(rs.first());
			if (rs.first() == false) {
				stmt.executeUpdate(cmdSQL);
				stmt.close();
				return true;
				// JOptionPane.showMessageDialog(null,
				// "Cadastro efetuado com sucesso!", "Confirma��o do Sistema",
				// JOptionPane.INFORMATION_MESSAGE);
			} else {
				// JOptionPane.showMessageDialog(null,
				// "Falha ao cadastrar. Matricula j� cadastrada no sistema!",
				// "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);

			}
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			// JOptionPane.showMessageDialog(null,"Erro ao cadastrar pessoa."+e.getMessage());
			e.getStackTrace();
			return false;
		}
		return true;
	}

	public boolean altera(Curso c) {
		if (c != null) {
			String comandoSQL = "UPDATE curso SET nome='" + c.getNome() + "', media='" + c.getMedia()
					+ "' WHERE id = " + c.getId();
			System.out.println(comandoSQL);
			try {
				java.sql.Statement stmt = (Statement) Conexao.getStatement();
				stmt.executeUpdate(comandoSQL);
				// JOptionPane.showMessageDialog(null,
				// "Altera��o de cadastro efetuada com sucesso!",
				// "Confirma��o do Sistema", JOptionPane.INFORMATION_MESSAGE);
				stmt.close();
			} catch (SQLException e) {
				// JOptionPane.showMessageDialog(null,"Erro ao alterar cadastro."+e.getMessage());
				e.getStackTrace();
				return false;
			}
		} else {
			// throw new MedicoErro();
		}
		return true;
	}

	public List busca(String nome) {
		ResultSet rs;
		List cursos = new ArrayList<Curso>();
		String comandoSQL;
		comandoSQL = "select * from curso where nome like'%" + nome + "%'";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next()) {
				Curso c = new Curso(); // rs.getInt("id"),rs.getString("nome")
				c.setId(rs.getInt("id"));
				c.setNome(rs.getString("nome"));
				c.setMedia(rs.getFloat("media"));
				cursos.add(c);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println("e");
			e.printStackTrace();
		}
		return cursos;
	}

	public Curso busca(int id) {
		ResultSet rs;
		String comandoSQL;
		comandoSQL = "SELECT * FROM curso WHERE id LIKE '" + id + "'";
		System.out.println(comandoSQL);
		try {
			Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			rs.next();
			Curso c = new Curso(); // rs.getInt("crm"),rs.getString("nome")
			c.setId(rs.getInt("id"));
			c.setNome(rs.getString("nome"));
			c.setMedia(rs.getFloat("media"));
			stmt.close();
			return c;

		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null, "Erro no pesquisar por id!");
			e.printStackTrace();
			System.out.println(e);
		}

		return null;
	}

	public ArrayList<Float> buscaMedias() {
		ResultSet rs;
		ArrayList<Float> medias = new ArrayList<Float>();
		String comandoSQL;
		comandoSQL = "select media from curso";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next())
				medias.add(rs.getFloat("media"));
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println(e);
			e.printStackTrace();
		}
		return medias;
	}

	public ArrayList<Curso> lista() {
		System.out.println("Pegando lista com os cursos");
		ResultSet rs;
		ArrayList<Curso> cursos = new ArrayList<Curso>();
		String comandoSQL;
		comandoSQL = "select * from curso ORDER BY nome";
		System.out.println(comandoSQL);
		try {
			java.sql.Statement stmt = Conexao.getStatement();
			rs = stmt.executeQuery(comandoSQL);
			while (rs.next()) {
				Curso c = new Curso();
				c.setId(rs.getInt("id"));
				c.setNome(rs.getString("nome"));
				c.setMedia(rs.getFloat("media"));
				cursos.add(c);
			}
			stmt.close();
		} catch (SQLException e) {
			// JOptionPane.showMessageDialog(null,"Erro no listar!");
			System.out.println(e);
			e.printStackTrace();
		}
		return cursos;
	}
}
