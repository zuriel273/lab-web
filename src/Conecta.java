import java.util.Iterator;
import java.util.List;

import model.Curso;
import model.Disciplina;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;



import dados.HibernateUtil;


public class Conecta {
	public static void main(String[] args) {
		Session session = null;
		Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
				
		try {  
			trns = session.beginTransaction();       
				Disciplina c = new Disciplina();       
				c.setMedia(10);  
				c.setNome("Hiber23");   
				c.setCodigo("HI23");
				session.save(c);       
				session.getTransaction().commit(); 
				} 
		    catch (RuntimeException e) 
		       {   
		    	if(trns != null){    trns.rollback();   }  
		    	e.printStackTrace();  } 
			finally{   session.flush();   session.close();   
			
			}
		
		/*  try {  
			trns = session.beginTransaction();       
				Curso c = new Curso();       
				c.setMedia(10);  
				c.setNome("Hiber");       
				session.save(c);       
				session.getTransaction().commit(); 
				} 
		    catch (RuntimeException e) 
		       {   
		    	if(trns != null){    trns.rollback();   }  
		    	e.printStackTrace();  } 
			finally{   session.flush();   session.close();   
			
			} */
		
		/*	
		try {   
		
		trns = session.beginTransaction();   
		List<Curso> cursos = session.createQuery("from Curso as u ").list();   
		for (Iterator<Curso> iter = cursos.iterator(); iter.hasNext();) 
		{    
			Curso c1 = iter.next();    
			System.out.println(c1.getNome() +" " + c1.getMedia());   }   
		trns.commit();  } 
		catch (RuntimeException e) {   if(trns != null){    trns.rollback();   }   e.printStackTrace();  } 
		finally{   session.flush();   session.close();  }
	}*/
	
//	try {   
//		
//		trns = session.beginTransaction();   
//		List<Curso> cursos = session.createQuery("from Curso as u ").list();   
//		for (Iterator<Curso> iter = cursos.iterator(); iter.hasNext();) 
//		{    
//			Curso c1 = iter.next();    
//			System.out.println(c1.getNome() +" " + c1.getMedia());   }   
//		trns.commit();  } 
//		catch (RuntimeException e) {   if(trns != null){    trns.rollback();   }   e.printStackTrace();  } 
//		finally{   session.flush();   session.close();  }
	}
}
