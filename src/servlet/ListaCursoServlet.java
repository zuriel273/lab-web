package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import model.Curso;

import dados.CursoDAO;
import dados.HibernateUtil;

@WebServlet(name = "ListaCurso", urlPatterns = { "/listaCurso" })
public class ListaCursoServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		
		Session session = null;
		Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
		
		PrintWriter out = response.getWriter();
					
		trns = session.beginTransaction();
		ArrayList<Curso> listaCurso = (ArrayList<Curso>) session.createQuery("from Curso as u ").list();
				
		request.setAttribute("lista", listaCurso);		
		
		RequestDispatcher view = getServletContext().getRequestDispatcher("/jsp/lista_curso.jsp");
		
		view.forward(request,response);	
		
		out.close();		
	}
}
