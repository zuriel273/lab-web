package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Curso;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

import dados.HibernateUtil;

/**
 * Servlet implementation class Media
 */
@WebServlet("/Media")
public class Media extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Media() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Session session = null;
		//Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
		
		PrintWriter out = response.getWriter();
					
		//trns = session.beginTransaction();
		ArrayList<Curso> listaCurso = (ArrayList<Curso>) session.createQuery("from Curso as u ").list();
		JSONArray jArray = new JSONArray();
		
		for (Curso c : listaCurso){
			JSONObject jObject = new JSONObject();
			try {
				jObject.put("nome", c.getNome());
				jObject.put("media", c.getMedia());
			} catch (JSONException e) {
				e.printStackTrace();
			}		
			
			jArray.put(jObject);
		}
		
		JSONObject jObject = new JSONObject();
		try {
			jObject.put("data", jArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}		
		
		response.getWriter().write(jObject.toString());
		out.close();
	}
}
