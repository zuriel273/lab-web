package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import dados.CursoDAO;
import dados.HibernateUtil;
import model.Curso;

@WebServlet(name = "Adiciona Curso", urlPatterns = { "/adicionaCurso" })
public class AdicionaCursoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Session session = null;
		Transaction trns = null;  
		session = HibernateUtil.getSession().openSession();
		
		String nome = request.getParameter("nome");
		float media = 0;
		Curso curso = new Curso();
		curso.setNome(nome);
		curso.setMedia(media);
		
		PrintWriter out = response.getWriter();
			
		try {  
				trns = session.beginTransaction();       
				session.save(curso);       
				session.getTransaction().commit(); 
				out.println("<script>alert('Curso cadastrado com sucesso.');location.href='index_teste_curso.html';</script>");
				} 
		    catch (RuntimeException e) 
		       {
		    	out.println("<script>alert('Erro ao cadastrar Curso.');location.href='index_teste_curso.html';</script>");
		    	if(trns != null){    trns.rollback();   }  
		    	e.printStackTrace();  } 
			finally{   session.flush();   session.close();   
			
			}
		
		}

}
