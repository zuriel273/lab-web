package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import dados.CursoDAO;
import dados.HibernateUtil;
import model.Curso;

@WebServlet(name = "AlteraCurso", urlPatterns = { "/alteraCurso" })
public class AlteraCursoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		RequestDispatcher view = getServletContext().getRequestDispatcher("/jsp/altera_curso.jsp");
    
		view.forward(request,response);	
		
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String nome = request.getParameter("nome");
		
		Curso curso = new Curso();
		
		Transaction trns = null;  
		Session session = HibernateUtil.getSession().openSession();
				
		curso.setNome(nome);
		curso.setId(id);
		
		PrintWriter out = response.getWriter();
		
		try {
			trns = session.beginTransaction();   
		    String hqlUpdate = "update Curso u set u.nome = :newName where u.id = :oldId";
		    int updatedEntities = session.createQuery( hqlUpdate )   
		    		 .setString( "newName", nome )  
		    		 .setInteger( "oldId", id )   
		    		 .executeUpdate();    
		     trns.commit();  
		     out.println("<script>alert('Curso alterado com sucesso.');location.href='listaCurso';</script>");
		     
		} catch (RuntimeException e) 
		     {   
			out.println("<script>alert('Erro ao alterar Curso.');location.href='listaCurso';</script>");
			if(trns != null){    trns.rollback();   }  
		     e.printStackTrace();  } 
		    finally{   session.flush();   session.close();  
		    
		    } 		
		
			
		 
					
				
		out.close();		
	}

}
