<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.Curso" %>  
<%@ page import="java.util.ArrayList" %>      


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="layout/layout.css" rel="stylesheet" type="text/css">
<title>Lista de Cursos</title>
</head>
<body>
       <h1>Lista de Cursos</h1>  
       <table border="0" align="center" >  
            <tr>
              <td colspan="4"><hr></td>
            </tr>     
            <tr>
              <td><br></td>
            </tr>
            <tr>  
                <th>NOME</th>  
                <th>MEDIA</th>  
            </tr>  
              <%                                            
                ArrayList<Curso> lista = (ArrayList)request.getAttribute("lista");
              	Curso c;
              	if(lista != null) {
                	for (int i = 0; i < lista.size(); i++ ) {
                		c = (Curso)lista.get(i);
              %>  
            <tr>   
                <td><%=((Curso)lista.get(i)).getNome()%></td>
                <td><%=((Curso)lista.get(i)).getMedia()%></td>
                <td><a href="alteraCurso?nome=<%=c.getNome()%>&id=<%=c.getId()%>">Alterar</a> </td>  
            </tr>    
             <%  
                	}  
              	}
              %>  
        </table>
        <br><br>
        <input type="button" onclick="location.href='loged.jsf'" value="Voltar"/>
</body>
</html>