# Comandos GIT

## CONFIGURANDO RSA

### 1.
	$ cd ~/.ssh
	$ ls
	
>Caso haja alguma pasta ou algum arquivo rsa.pub vá para o 3

### 2.
	ssh-keygen -t rsa -C "your_email@example.com"
	*Criando sua chave com seu email*
	Enter file in which to save the key (/c/Users/you/.ssh/id_rsa): [ENTER]
	Enter passphrase (empty for no passphrase): [SUA SENHA]
	Enter same passphrase again: [SUA SENHA DENOVO]

### 3.
	notepad ~/.ssh/id_rsa.pub
	OU
	gedit ~/.ssh/id_rsa.pub
	*Copie o conteudo que aparece no bloco de notas*

### 4.
	No BitBucket se logue
	Va no Menu > Gerencia Conta > Chaves SSH
	Click em *adiconar chave* (ou importa chaves do GitHub)
	Cole sua chave e de um nome para aquele cadastro de chave
	Depois click em *adicionar chave*

## SUBIR

	$ git add *
	$ git commit -m "Comente sua alteração"
	$ git push -u origin master

## DESCER

	git pull origin
